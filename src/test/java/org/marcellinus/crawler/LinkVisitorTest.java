package org.marcellinus.crawler;

import org.junit.Before;
import org.junit.Test;
import org.marcellinus.conf.ICrawlerConfiguration;
import org.marcellinus.utils.CrawlerUtils;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.assertTrue;

/**
 * @author Eric Eiswerth
 */
public class LinkVisitorTest {

    @Mock
    private ICrawlerConfiguration configuration;

    private LinkVisitor linkVisitor;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        linkVisitor = new LinkVisitor();

        final CrawlerUtils crawlerUtils = new CrawlerUtils();
        ReflectionTestUtils.setField(crawlerUtils, "configuration", configuration);
        ReflectionTestUtils.setField(linkVisitor, "crawlerUtils", crawlerUtils);
    }

    @Test
    public void shouldContainURLWithHash() {
        String baseUrl = "http://www.test.com/";
        linkVisitor.markUrlAsVisited(baseUrl);

        assertTrue(linkVisitor.hasVisitedUrl(baseUrl));
        assertTrue(linkVisitor.hasVisitedUrl(baseUrl + "#"));
        assertTrue(linkVisitor.hasVisitedUrl(baseUrl + "#test1"));
        assertTrue(linkVisitor.hasVisitedUrl(baseUrl + "#test2"));
    }

}
