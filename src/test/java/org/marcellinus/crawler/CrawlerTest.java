package org.marcellinus.crawler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.hamcrest.CoreMatchers;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.marcellinus.conf.ICrawlerConfiguration;
import org.marcellinus.crawler.helpers.MockPageLinkEntityService;
import org.marcellinus.crawler.interceptors.ILinkInterceptor;
import org.marcellinus.exceptions.CrawlerException;
import org.marcellinus.model.LinkEntity;
import org.marcellinus.model.LinkFactory;
import org.marcellinus.model.PageLinkEntity;
import org.marcellinus.persistence.exceptions.EntityServiceException;
import org.marcellinus.persistence.service.IEntityService;
import org.marcellinus.utils.CrawlerUtils;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.openqa.selenium.WebDriver;
import org.springframework.integration.MessageChannel;
import org.springframework.test.util.ReflectionTestUtils;

import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * @author Eric Eiswerth
 */
public class CrawlerTest {

    private static final String DEFAULT_URL = "http://www.test.com";

    @Mock
    private ICrawlerConfiguration configuration;

    @Mock
    private PageView pageView;

    @Mock
    private MessageChannel messageChannel;

    @Mock
    private WebDriver driver;

    @Mock
    private WebDriver.Navigation navigation;

    @Mock
    private ILinkInterceptor interceptor;

    private IEntityService<PageLinkEntity> pageLinkEntityService;

    private LinkVisitor linkVisitor;

    private LinkFactory linkFactory;

    private Crawler crawler;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        crawler = new Crawler();
        crawler.addLinkInterceptor(interceptor);

        // Wire the non-mock objects.
        pageLinkEntityService = new MockPageLinkEntityService();
        linkVisitor = new LinkVisitor();
        linkFactory = new LinkFactory();
        final LinkValidator linkValidator = new LinkValidator(DEFAULT_URL);
        final CrawlerUtils crawlerUtils = new CrawlerUtils();
        ReflectionTestUtils.setField(crawler, "linkValidator", linkValidator);
        ReflectionTestUtils.setField(crawler, "linkVisitor", linkVisitor);
        ReflectionTestUtils.setField(crawler, "linkFactory", linkFactory);
        ReflectionTestUtils.setField(crawler, "crawlerUtils", crawlerUtils);
        ReflectionTestUtils.setField(linkFactory, "crawlerUtils", crawlerUtils);
        ReflectionTestUtils.setField(linkVisitor, "crawlerUtils", crawlerUtils);
        ReflectionTestUtils.setField(crawlerUtils, "configuration", configuration);

        // Wire the mock objects.
        ReflectionTestUtils.setField(crawler, "configuration", configuration);
        ReflectionTestUtils.setField(crawler, "pageLinkEntityService", pageLinkEntityService);
        ReflectionTestUtils.setField(crawler, "pageView", pageView);
        ReflectionTestUtils.setField(crawler, "messageChannel", messageChannel);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCrawlWithZeroMaxLinksConfiguration() throws CrawlerException {
        when(configuration.getMaxLinksToCrawl()).thenReturn(0);
        crawler.crawl(driver);
    }

    @Test
    public void crawlSinglePageWithNoLinks() throws CrawlerException, EntityServiceException {
        final PageLinkEntity mockPageLinkEntity = linkFactory.getPageLinkEntity(DEFAULT_URL, DEFAULT_URL, false);
        when(configuration.getMaxLinksToCrawl()).thenReturn(-1);
        when(driver.navigate()).thenReturn(navigation);
        when(configuration.getBaseUrl()).thenReturn(DEFAULT_URL);
        when(driver.getCurrentUrl()).thenReturn(DEFAULT_URL);

        when(pageView.getAllUrlsFromPage(driver)).thenReturn(Collections.EMPTY_LIST);

        crawler.crawl(driver);

        verify(navigation, times(2)).to(DEFAULT_URL);
        verify(interceptor, times(1)).handleLink(any(PageLinkEntity.class), any(WebDriver.class));

        final List<PageLinkEntity> entities = pageLinkEntityService.list();
        assertTrue(entities.size() == 1);
        assertKeyIsPresent(entities, mockPageLinkEntity);
        assertTrue(linkVisitor.hasVisitedUrl(DEFAULT_URL));
        assertTrue(linkVisitor.getNumberOfLinksVisited() == 1);

        final PageLinkEntity mainPageLinkEntity = pageLinkEntityService.read(mockPageLinkEntity.getLinkID());
        assertNotNull(mainPageLinkEntity);
        assertNull(mainPageLinkEntity.getRedirectedFromUrl());
        assertTrue(mainPageLinkEntity.getIncomingLinks().isEmpty());
        assertTrue(mainPageLinkEntity.getOutgoingLinks().isEmpty());
    }

    @Test
    public void crawlPageWithThreeChildLeafLinks() throws CrawlerException, EntityServiceException {
        final PageLinkEntity mockPageLinkEntity = linkFactory.getPageLinkEntity(DEFAULT_URL, DEFAULT_URL, false);
        when(configuration.getMaxLinksToCrawl()).thenReturn(-1);
        when(driver.navigate()).thenReturn(navigation);
        when(configuration.getBaseUrl()).thenReturn(DEFAULT_URL);

        final String url1 = DEFAULT_URL + "/1";
        final String url2 = DEFAULT_URL + "/2";
        final String url3 = DEFAULT_URL + "/3";
        final List<String> urls = new ArrayList<>(3);
        urls.add(url1);
        urls.add(url2);
        urls.add(url3);

        when(driver.getCurrentUrl()).thenReturn(DEFAULT_URL).thenReturn(url1).thenReturn(url2).thenReturn(url3);
        when(pageView.getAllUrlsFromPage(driver)).thenReturn(urls).thenReturn(Collections.EMPTY_LIST)
                .thenReturn(Collections.EMPTY_LIST).thenReturn(Collections.EMPTY_LIST);

        crawler.crawl(driver);

        verify(navigation, times(5)).to(anyString());
        verify(interceptor, times(4)).handleLink(any(PageLinkEntity.class), any(WebDriver.class));

        final List<PageLinkEntity> entities = pageLinkEntityService.list();
        final PageLinkEntity mockPageLinkEntity1 = linkFactory.getPageLinkEntity(url1, url1, false);
        final PageLinkEntity mockPageLinkEntity2 = linkFactory.getPageLinkEntity(url2, url2, false);
        final PageLinkEntity mockPageLinkEntity3 = linkFactory.getPageLinkEntity(url3, url3, false);

        assertTrue(entities.size() == 4);
        assertKeyIsPresent(entities, mockPageLinkEntity);
        assertKeyIsPresent(entities, mockPageLinkEntity1);
        assertKeyIsPresent(entities, mockPageLinkEntity2);
        assertKeyIsPresent(entities, mockPageLinkEntity3);
        assertTrue(linkVisitor.hasVisitedUrl(DEFAULT_URL));
        assertTrue(linkVisitor.hasVisitedUrl(url1));
        assertTrue(linkVisitor.hasVisitedUrl(url2));
        assertTrue(linkVisitor.hasVisitedUrl(url3));
        assertTrue(linkVisitor.getNumberOfLinksVisited() == 4);

        final PageLinkEntity mainPageLinkEntity = pageLinkEntityService.read(mockPageLinkEntity.getLinkID());
        assertNotNull(mainPageLinkEntity);
        assertNull(mainPageLinkEntity.getRedirectedFromUrl());
        assertTrue(mainPageLinkEntity.getIncomingLinks().isEmpty());
        assertFalse(mainPageLinkEntity.getOutgoingLinks().isEmpty());
        assertKeyIsPresent(mainPageLinkEntity.getOutgoingLinks(), mockPageLinkEntity1);
        assertKeyIsPresent(mainPageLinkEntity.getOutgoingLinks(), mockPageLinkEntity2);
        assertKeyIsPresent(mainPageLinkEntity.getOutgoingLinks(), mockPageLinkEntity3);

        final PageLinkEntity pageLinkEntity1 = pageLinkEntityService.read(mockPageLinkEntity1.getLinkID());
        assertNotNull(pageLinkEntity1);
        assertNull(pageLinkEntity1.getRedirectedFromUrl());
        assertFalse(pageLinkEntity1.getIncomingLinks().isEmpty());
        assertKeyIsPresent(pageLinkEntity1.getIncomingLinks(), mainPageLinkEntity);
        assertTrue(pageLinkEntity1.getOutgoingLinks().isEmpty());

        final PageLinkEntity pageLinkEntity2 = pageLinkEntityService.read(mockPageLinkEntity2.getLinkID());
        assertNotNull(pageLinkEntity2);
        assertNull(pageLinkEntity2.getRedirectedFromUrl());
        assertFalse(pageLinkEntity2.getIncomingLinks().isEmpty());
        assertKeyIsPresent(pageLinkEntity2.getIncomingLinks(), mainPageLinkEntity);
        assertTrue(pageLinkEntity2.getOutgoingLinks().isEmpty());

        final PageLinkEntity pageLinkEntity3 = pageLinkEntityService.read(mockPageLinkEntity3.getLinkID());
        assertNotNull(pageLinkEntity3);
        assertNull(pageLinkEntity3.getRedirectedFromUrl());
        assertFalse(pageLinkEntity3.getIncomingLinks().isEmpty());
        assertKeyIsPresent(pageLinkEntity3.getIncomingLinks(), mainPageLinkEntity);
        assertTrue(pageLinkEntity3.getOutgoingLinks().isEmpty());
    }

    @Test
    public void crawlSingleLinkPagesThatGoThreeLevelsDeep() throws CrawlerException, EntityServiceException {
        final PageLinkEntity mockPageLinkEntity = linkFactory.getPageLinkEntity(DEFAULT_URL, DEFAULT_URL, false);
        when(configuration.getMaxLinksToCrawl()).thenReturn(-1);
        when(driver.navigate()).thenReturn(navigation);
        when(configuration.getBaseUrl()).thenReturn(DEFAULT_URL);

        final String url1 = DEFAULT_URL + "/1";
        final String url2 = DEFAULT_URL + "/1/2";
        final String url3 = DEFAULT_URL + "/1/3";
        final String url4 = "http://foo.com";

        final List<String> mainPageUrls = new ArrayList<>(1);
        mainPageUrls.add(url1);
        // Add this invalid URL to ensure it's not included in the results.
        mainPageUrls.add(url4);
        final List<String> page1Urls = new ArrayList<>(1);
        page1Urls.add(url2);
        final List<String> page2Urls = new ArrayList<>(1);
        page2Urls.add(url3);

        when(driver.getCurrentUrl()).thenReturn(DEFAULT_URL).thenReturn(url1).thenReturn(url2).thenReturn(url3);
        when(pageView.getAllUrlsFromPage(driver)).thenReturn(mainPageUrls).thenReturn(page1Urls).thenReturn(page2Urls);

        crawler.crawl(driver);

        verify(navigation, times(5)).to(anyString());
        verify(interceptor, times(4)).handleLink(any(PageLinkEntity.class), any(WebDriver.class));

        final List<PageLinkEntity> entities = pageLinkEntityService.list();
        final PageLinkEntity mockPageLinkEntity1 = linkFactory.getPageLinkEntity(url1, url1, false);
        final PageLinkEntity mockPageLinkEntity2 = linkFactory.getPageLinkEntity(url2, url2, false);
        final PageLinkEntity mockPageLinkEntity3 = linkFactory.getPageLinkEntity(url3, url3, false);

        assertTrue(entities.size() == 4);

        assertKeyIsPresent(entities, mockPageLinkEntity);
        assertKeyIsPresent(entities, mockPageLinkEntity1);
        assertKeyIsPresent(entities, mockPageLinkEntity2);
        assertKeyIsPresent(entities, mockPageLinkEntity3);
        assertTrue(linkVisitor.hasVisitedUrl(DEFAULT_URL));
        assertTrue(linkVisitor.hasVisitedUrl(url1));
        assertTrue(linkVisitor.hasVisitedUrl(url2));
        assertTrue(linkVisitor.hasVisitedUrl(url3));
        assertTrue(linkVisitor.getNumberOfLinksVisited() == 4);

        final PageLinkEntity mainPageLinkEntity = pageLinkEntityService.read(mockPageLinkEntity.getLinkID());
        assertNotNull(mainPageLinkEntity);
        assertNull(mainPageLinkEntity.getRedirectedFromUrl());
        assertTrue(mainPageLinkEntity.getIncomingLinks().isEmpty());
        assertFalse(mainPageLinkEntity.getOutgoingLinks().isEmpty());
        assertKeyIsPresent(mainPageLinkEntity.getOutgoingLinks(), mockPageLinkEntity1);

        final PageLinkEntity pageLinkEntity1 = pageLinkEntityService.read(mockPageLinkEntity1.getLinkID());
        assertNotNull(pageLinkEntity1);
        assertNull(pageLinkEntity1.getRedirectedFromUrl());
        assertFalse(pageLinkEntity1.getIncomingLinks().isEmpty());
        assertKeyIsPresent(pageLinkEntity1.getIncomingLinks(), mainPageLinkEntity);
        assertFalse(pageLinkEntity1.getOutgoingLinks().isEmpty());
        assertKeyIsPresent(pageLinkEntity1.getOutgoingLinks(), mockPageLinkEntity2);

        final PageLinkEntity pageLinkEntity2 = pageLinkEntityService.read(mockPageLinkEntity2.getLinkID());
        assertNotNull(pageLinkEntity2);
        assertNull(pageLinkEntity2.getRedirectedFromUrl());
        assertFalse(pageLinkEntity2.getIncomingLinks().isEmpty());
        assertKeyIsPresent(pageLinkEntity2.getIncomingLinks(), mockPageLinkEntity1);
        assertFalse(pageLinkEntity2.getOutgoingLinks().isEmpty());
        assertKeyIsPresent(pageLinkEntity2.getOutgoingLinks(), mockPageLinkEntity3);

        final PageLinkEntity pageLinkEntity3 = pageLinkEntityService.read(mockPageLinkEntity3.getLinkID());
        assertNotNull(pageLinkEntity3);
        assertNull(pageLinkEntity3.getRedirectedFromUrl());
        assertFalse(pageLinkEntity3.getIncomingLinks().isEmpty());
        assertKeyIsPresent(pageLinkEntity3.getIncomingLinks(), mockPageLinkEntity2);
        assertTrue(pageLinkEntity3.getOutgoingLinks().isEmpty());
    }

    @Test
    public void crawlSinglePageWithNoLinksThatRedirects() throws CrawlerException, EntityServiceException {
        final PageLinkEntity mockPageLinkEntity = linkFactory.getPageLinkEntity(DEFAULT_URL, DEFAULT_URL, false);
        when(configuration.getMaxLinksToCrawl()).thenReturn(-1);
        when(driver.navigate()).thenReturn(navigation);
        when(configuration.getBaseUrl()).thenReturn(DEFAULT_URL);

        final String url1 = DEFAULT_URL + "/1";
        when(driver.getCurrentUrl()).thenReturn(url1);

        when(pageView.getAllUrlsFromPage(driver)).thenReturn(Collections.EMPTY_LIST);

        crawler.crawl(driver);

        verify(navigation, times(2)).to(DEFAULT_URL);
        verify(interceptor, times(1)).handleLink(any(PageLinkEntity.class), any(WebDriver.class));

        final List<PageLinkEntity> entities = pageLinkEntityService.list();
        assertTrue(entities.size() == 1);
        assertKeyIsPresent(entities, mockPageLinkEntity);
        assertTrue(linkVisitor.hasVisitedUrl(DEFAULT_URL));
        assertTrue(linkVisitor.getNumberOfLinksVisited() == 2);

        // The entity should be retrievable via the initial link ID instance.
        final PageLinkEntity mainPageLinkEntity = pageLinkEntityService.read(mockPageLinkEntity.getLinkID());
        assertNotNull(mainPageLinkEntity);
        // The entity's URL should be the redirected URL, instead of the original URL.
        assertEquals(url1, mainPageLinkEntity.getUrl());
        // The "redirected from" URL should be the original URL.
        assertEquals(DEFAULT_URL, mainPageLinkEntity.getRedirectedFromUrl());
        assertTrue(mainPageLinkEntity.getIncomingLinks().isEmpty());
        assertTrue(mainPageLinkEntity.getOutgoingLinks().isEmpty());
    }

    private void assertKeyIsPresent(Collection<LinkEntity> linkEntities, LinkEntity linkEntity) {
        assertThat(linkEntities, hasItem(linkEntityKeyValue(linkEntity.getKey())));
    }

    private void assertKeyIsPresent(Collection<PageLinkEntity> linkEntities, PageLinkEntity linkEntity) {
        assertThat(linkEntities, hasItem(pageLinkEntityKeyValue(linkEntity.getKey())));
    }

    private Matcher<LinkEntity> linkEntityKeyValue(String expected) {
        return new FeatureMatcher<LinkEntity, String>(CoreMatchers.equalTo(expected), "key value", "key") {
            @Override
            protected String featureValueOf(LinkEntity linkEntity) {
                return linkEntity.getKey();
            }
        };
    }

    private Matcher<PageLinkEntity> pageLinkEntityKeyValue(String expected) {
        return new FeatureMatcher<PageLinkEntity, String>(CoreMatchers.equalTo(expected), "key value", "key") {
            @Override
            protected String featureValueOf(PageLinkEntity linkEntity) {
                return linkEntity.getKey();
            }
        };
    }

}
