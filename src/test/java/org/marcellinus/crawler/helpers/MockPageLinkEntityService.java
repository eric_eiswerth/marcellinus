package org.marcellinus.crawler.helpers;

import java.util.*;
import org.marcellinus.model.LinkID;
import org.marcellinus.model.PageLinkEntity;
import org.marcellinus.persistence.exceptions.EntityServiceException;
import org.marcellinus.persistence.service.IEntityService;

/**
 * This class can be used in place of mocking instances of the {@link IEntityService}.
 *
 * @author Eric Eiswerth
 */
public class MockPageLinkEntityService implements IEntityService<PageLinkEntity> {

    private final Map<LinkID, PageLinkEntity> entityMap = new HashMap<>();

    @Override
    public void create(PageLinkEntity pageLinkEntity) throws EntityServiceException {
        Objects.requireNonNull(pageLinkEntity, "Entity cannot be null.");
        entityMap.put(pageLinkEntity.getLinkID(), pageLinkEntity);
    }

    @Override
    public PageLinkEntity read(LinkID linkID) throws EntityServiceException {
        Objects.requireNonNull(linkID, "Link ID cannot be null.");
        return entityMap.get(linkID);
    }

    @Override
    public List<PageLinkEntity> list() throws EntityServiceException {
        final List<PageLinkEntity> entities = new ArrayList<>(entityMap.values().size());
        entities.addAll(entityMap.values());
        return entities;
    }
}
