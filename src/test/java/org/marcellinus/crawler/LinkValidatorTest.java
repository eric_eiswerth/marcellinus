package org.marcellinus.crawler;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Unit test for the {@link LinkValidator} class.
 *
 * @author Eric Eiswerth
 */
public class LinkValidatorTest {

    @Test
    public void shouldBeValidLinksWithStandardBaseUrl() {
        final LinkValidator linkValidator = new LinkValidator("http://www.test.com");
        validAssertions(linkValidator);
    }

    @Test
    public void shouldBeValidLinksWithBaseUrlThatPointsToASpecificPage() {
        final LinkValidator linkValidator = new LinkValidator("http://www.test.com/test/foo.do");
        validAssertions(linkValidator);
    }

    @Test
    public void shouldBeValidLinksWithBaseUrlThatPointsToASpecificPageWithParameters() {
        final LinkValidator linkValidator = new LinkValidator("http://www.test.com/test/foo.do?abc=123");
        validAssertions(linkValidator);
    }

    @Test
    public void shouldBeValidLinksWithBaseUrlWithParameters() {
        final LinkValidator linkValidator = new LinkValidator("http://www.test.com?abc=123");
        validAssertions(linkValidator);
    }

    @Test
    public void shouldBeInvalidLinks() {
        final LinkValidator linkValidator = new LinkValidator("http://www.test.com");

        assertFalse(linkValidator.isValid(""));
        assertFalse(linkValidator.isValid("http://www.sub1.test.com"));
        assertFalse(linkValidator.isValid("http://www.sub2.test.com"));
        assertFalse(linkValidator.isValid("http://sub.telus.com"));
        assertFalse(linkValidator.isValid("http://sub2.telus.com"));
    }

    private void validAssertions(LinkValidator linkValidator) {
        assertTrue(linkValidator.isValid("www.test.com"));
        assertTrue(linkValidator.isValid("http://test.com/test"));
        assertTrue(linkValidator.isValid("https://test.com"));
        assertTrue(linkValidator.isValid("https://test.com?foo=test&foo2=test"));
        assertTrue(linkValidator.isValid("test.jsp"));
        assertTrue(linkValidator.isValid("/content/help/test/"));
        assertTrue(linkValidator.isValid("https://www.test.com/path/PATH/Login?parm1=abc&param2=https%3A%2F%2Fwww.test.com%3A443%2Faaabbbccc%2Fgo.do"));
    }

}
