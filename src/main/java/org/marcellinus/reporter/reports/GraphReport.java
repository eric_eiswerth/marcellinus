package org.marcellinus.reporter.reports;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.velocity.VelocityContext;
import org.marcellinus.exceptions.ReportException;
import org.marcellinus.model.LinkEntity;
import org.marcellinus.model.PageLinkEntity;
import org.marcellinus.reporter.reports.json.LinkEntityNode;
import org.marcellinus.reporter.reports.json.LinkEntityNodeFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Eric Eiswerth
 */
public class GraphReport implements IAggregatePageLinkEntityReport {

    private static final Logger LOGGER = LoggerFactory.getLogger(GraphReport.class);

    private final ObjectMapper mapper = new ObjectMapper();

    private final Map<String, LinkEntityNode> nodes = new HashMap<>();

    @Autowired
    private LinkEntityNodeFactory nodeFactory;

    private LinkEntityNode root;

    private String json;

    @Override
    public void handlePageLinkEntity(PageLinkEntity pageLinkEntity) throws ReportException {
        LinkEntityNode jsonObj = nodes.get(pageLinkEntity.getKey());
        if (jsonObj == null) {
            // We haven't seen this node yet.
            jsonObj = nodeFactory.getLinkEntityGraphJSON(pageLinkEntity);
        }
        if (root == null) {
            root = jsonObj;
        } else if (jsonObj.getId().equals(root.getId())) {
            // This is the root again. To protect against cycles, break out.
            return;
        } else {
            for (LinkEntity linkEntity : pageLinkEntity.getIncomingLinks()) {
                LinkEntityNode node = nodes.get(linkEntity.getKey());
                if (node == null) {
                    node = nodeFactory.getLinkEntityGraphJSON(linkEntity);
                    nodes.put(linkEntity.getKey(), node);
                }
                node.addChild(jsonObj);
            }
        }
        nodes.put(pageLinkEntity.getKey(), jsonObj);
    }

    @Override
    public void populateContext(VelocityContext context) {
        try {
            json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root);
            context.put("data", json);
        } catch (IOException ioe) {
            LOGGER.error("Error creating report content.", ioe);
        }
    }

    @Override
    public String getTemplateName() {
        return "templates/graph_template.vm";
    }

    @Override
    public String getReportName() {
        return "graph.html";
    }

    @Override
    public String getPageTitle() {
        return "Graph";
    }

    @Override
    public void close() {
    }
}
