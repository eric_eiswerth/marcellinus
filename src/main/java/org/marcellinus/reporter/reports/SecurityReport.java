package org.marcellinus.reporter.reports;

import org.marcellinus.exceptions.ReportException;
import org.marcellinus.model.PageLinkEntity;
import org.marcellinus.reporter.filters.IPageLinkEntityFilter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Eric Eiswerth
 */
public class SecurityReport extends AbstractAggregateReport {

    @Autowired
    private IPageLinkEntityFilter securityFilter;

    @Override
    public void handlePageLinkEntity(PageLinkEntity pageLinkEntity) throws ReportException {
        if (securityFilter.filterPageLinkEntity(pageLinkEntity)) {
            pageLinks.add(pageLinkEntity);
        }
    }

    @Override
    public String getTemplateName() {
        return "templates/security_template.vm";
    }

    @Override
    public String getReportName() {
        return "insecurepages.html";
    }

    @Override
    public String getPageTitle() {
        return "Potentially Insecure";
    }
}
