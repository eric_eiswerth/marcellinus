package org.marcellinus.reporter.reports;

import org.marcellinus.exceptions.ReportException;
import org.marcellinus.model.PageLinkEntity;

/**
 * @author Eric Eiswerth
 */
public class AllLinksReport extends AbstractAggregateReport {

    @Override
    public void handlePageLinkEntity(PageLinkEntity pageLinkEntity) throws ReportException {
        if (pageLinkEntity != null) {
            pageLinks.add(pageLinkEntity);
        }
    }

    @Override
    public String getTemplateName() {
        return "templates/all_links_template.vm";
    }

    @Override
    public String getReportName() {
        return "alllinks.html";
    }

    @Override
    public String getPageTitle() {
        return "Visited URLs";
    }
}
