package org.marcellinus.reporter.reports;

import org.marcellinus.exceptions.ReportException;
import org.marcellinus.model.PageLinkEntity;
import org.marcellinus.reporter.filters.IPageLinkEntityFilter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Eric Eiswerth
 */
public class Http404Report extends AbstractAggregateReport {

    @Autowired
    private IPageLinkEntityFilter http404Filter;

    @Override
    public void handlePageLinkEntity(PageLinkEntity pageLinkEntity) throws ReportException {
        if (http404Filter.filterPageLinkEntity(pageLinkEntity)) {
            pageLinks.add(pageLinkEntity);
        }
    }

    @Override
    public String getTemplateName() {
        return "templates/404_template.vm";
    }

    @Override
    public String getReportName() {
        return "404pages.html";
    }

    @Override
    public String getPageTitle() {
        return "404 Errors";
    }
}
