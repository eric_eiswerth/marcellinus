package org.marcellinus.reporter.reports;

/**
 * @author Eric Eiswerth
 */
public interface IAggregatePageLinkEntityReport extends IPageLinkEntityReport {

    public void close();

}
