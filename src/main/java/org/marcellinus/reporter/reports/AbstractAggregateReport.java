package org.marcellinus.reporter.reports;

import java.util.Set;
import java.util.TreeSet;
import org.apache.velocity.VelocityContext;
import org.marcellinus.model.PageLinkEntity;

/**
 * @author Eric Eiswerth
 */
public abstract class AbstractAggregateReport implements IAggregatePageLinkEntityReport {

    protected final Set<PageLinkEntity> pageLinks = new TreeSet<>();

    @Override
    public void populateContext(VelocityContext context) {
        context.put("links", pageLinks);
    }

    @Override
    public void close() {
        pageLinks.clear();
    }

}
