package org.marcellinus.reporter.reports;

import org.marcellinus.exceptions.ReportException;
import org.marcellinus.model.SkippedLink;

/**
 * @author Eric Eiswerth
 */
public interface ISkippedLinkReport extends IReport {

    /**
     * Handles the given skipped link.
     * @param skippedLink
     * @throws ReportException
     */
    public void handleSkippedLink(SkippedLink skippedLink) throws ReportException;

}
