package org.marcellinus.reporter.reports;

import org.marcellinus.exceptions.ReportException;
import org.marcellinus.model.PageLinkEntity;

/**
 * @author Eric Eiswerth
 */
public interface IPageLinkEntityReport extends IReport {

    /**
     * Handles the given link. Links are provided to this method in the order in which they were visisted.
     * @param pageLinkEntity
     * @throws org.marcellinus.exceptions.ReportException
     */
    public void handlePageLinkEntity(PageLinkEntity pageLinkEntity) throws ReportException;

}
