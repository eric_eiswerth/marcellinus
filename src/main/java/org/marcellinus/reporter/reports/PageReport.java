package org.marcellinus.reporter.reports;

import org.apache.velocity.VelocityContext;
import org.marcellinus.model.PageLinkEntity;
import org.marcellinus.exceptions.ReportException;

/**
 * @author Eric Eiswerth
 */
public class PageReport implements ISimplePageLinkEntityReport {

    private PageLinkEntity pageLinkEntity;

    @Override
    public void handlePageLinkEntity(PageLinkEntity pageLinkEntity) throws ReportException {
        this.pageLinkEntity = pageLinkEntity;
    }

    @Override
    public void populateContext(VelocityContext context) {
        context.put("link", pageLinkEntity);
    }

    @Override
    public String getTemplateName() {
        return "templates/page_template.vm";
    }

    @Override
    public String getReportName() {
        return pageLinkEntity.getLink();
    }

    @Override
    public String getPageTitle() {
        return pageLinkEntity.getDescription();
    }
}
