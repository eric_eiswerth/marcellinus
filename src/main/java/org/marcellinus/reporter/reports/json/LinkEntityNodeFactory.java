package org.marcellinus.reporter.reports.json;

import org.marcellinus.model.LinkEntity;
import org.marcellinus.model.PageLinkEntity;
import org.springframework.stereotype.Component;

/**
 * @author Eric Eiswerth
 */
@Component("nodeFactory")
public class LinkEntityNodeFactory {

    public LinkEntityNode getLinkEntityGraphJSON(PageLinkEntity pageLinkEntity) {
        final LinkEntityNode json = new LinkEntityNode();
        json.setId(pageLinkEntity.getKey());
        json.setName(pageLinkEntity.getUrl());
        for (LinkEntity linkEntity : pageLinkEntity.getOutgoingLinks()) {
            json.addChild(getLinkEntityGraphJSON(linkEntity));
        }
        return json;
    }

    public LinkEntityNode getLinkEntityGraphJSON(LinkEntity linkEntity) {
        final LinkEntityNode json = new LinkEntityNode();
        json.setId(linkEntity.getKey());
        json.setName(linkEntity.getUrl());
        return json;
    }

}
