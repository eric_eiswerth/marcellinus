package org.marcellinus.reporter.reports.json;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Eric Eiswerth
 */
@JsonIdentityInfo(property="@key", generator=ObjectIdGenerators.IntSequenceGenerator.class)
public class LinkEntityNode {

    @JsonProperty("id")
    private String id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("children")
    private List<LinkEntityNode> children = new ArrayList<>();

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void addChild(LinkEntityNode linkEntity) {
        children.add(linkEntity);
    }

    public List<LinkEntityNode> getChildren() {
        return children;
    }
}
