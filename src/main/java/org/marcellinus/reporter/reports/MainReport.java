package org.marcellinus.reporter.reports;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import org.apache.velocity.VelocityContext;
import org.marcellinus.exceptions.ReportException;
import org.marcellinus.model.PageLinkEntity;

/**
 * @author Eric Eiswerth
 */
public class MainReport implements IAggregatePageLinkEntityReport {

    private final ObjectMapper mapper = new ObjectMapper();
    private final StringBuilder jsonSb = new StringBuilder();

    @Override
    public void handlePageLinkEntity(PageLinkEntity pageLinkEntity) throws ReportException {
        try {
            final String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(pageLinkEntity);
            if (jsonSb.length() > 0) {
                jsonSb.append(",\n");
            }
            jsonSb.append(json);
        } catch (IOException ioe) {
            throw new ReportException("Error creating report content.", ioe);
        }
    }

    @Override
    public void populateContext(VelocityContext context) {
        context.put("data", jsonSb.toString());
    }

    @Override
    public String getTemplateName() {
        return "templates/main_template.vm";
    }

    @Override
    public String getReportName() {
        return "report.html";
    }

    @Override
    public String getPageTitle() {
        return "Home";
    }

    @Override
    public void close() {
        jsonSb.delete(0, jsonSb.length());
    }
}
