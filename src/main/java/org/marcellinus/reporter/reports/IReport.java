package org.marcellinus.reporter.reports;

import org.apache.velocity.VelocityContext;

/**
 * @author Eric Eiswerth
 */
public interface IReport {

    /**
     * Populates the velocity context.
     * @param context
     */
    public void populateContext(VelocityContext context);

    /**
     * Gets the name of the template that will be used to create this report.
     * @return
     */
    public String getTemplateName();

    /**
     * Gets the report name. This is the name of the HTML file where the final content will be written to.
     * @return
     */
    public String getReportName();

    /**
     * Gets the page title. The title will be appended to "Crawler Report - ".
     * @return
     */
    public String getPageTitle();

}