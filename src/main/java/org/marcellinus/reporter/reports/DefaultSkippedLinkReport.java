package org.marcellinus.reporter.reports;

import java.util.Set;
import java.util.TreeSet;
import org.apache.velocity.VelocityContext;
import org.marcellinus.exceptions.ReportException;
import org.marcellinus.model.SkippedLink;

/**
 * @author Eric Eiswerth
 */
public class DefaultSkippedLinkReport implements ISkippedLinkReport {

    protected final Set<SkippedLink> pageLinks = new TreeSet<>();

    @Override
    public void handleSkippedLink(SkippedLink skippedLink) throws ReportException {
        if (skippedLink != null) {
            pageLinks.add(skippedLink);
        }
    }

    @Override
    public void populateContext(VelocityContext context) {
        context.put("links", pageLinks);
    }

    @Override
    public String getTemplateName() {
        return "templates/skipped_links_template.vm";
    }

    @Override
    public String getReportName() {
        return "skippedlinks.html";
    }

    @Override
    public String getPageTitle() {
        return "Skipped Links";
    }
}
