package org.marcellinus.reporter.reports;

import org.marcellinus.exceptions.ReportException;
import org.marcellinus.model.PageLinkEntity;
import org.marcellinus.reporter.filters.IPageLinkEntityFilter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Eric Eiswerth
 */
public class BrokenImagesReport extends AbstractAggregateReport {

    @Autowired
    private IPageLinkEntityFilter brokenImageFilter;

    @Override
    public void handlePageLinkEntity(PageLinkEntity pageLinkEntity) throws ReportException {
        if (brokenImageFilter.filterPageLinkEntity(pageLinkEntity)) {
            pageLinks.add(pageLinkEntity);
        }
    }

    @Override
    public String getTemplateName() {
        return "templates/broken_images_template.vm";
    }

    @Override
    public String getReportName() {
        return "brokenimagepages.html";
    }

    @Override
    public String getPageTitle() {
        return "Broken Images";
    }
}
