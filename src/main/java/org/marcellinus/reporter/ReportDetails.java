package org.marcellinus.reporter;

/**
 * @author Eric Eiswerth
 */
public class ReportDetails {

    private double totalTimeMinutes = 0;

    public double getTotalTimeMinutes() {
        return totalTimeMinutes;
    }

    public void setTotalTimeMinutes(double totalTimeMinutes) {
        this.totalTimeMinutes = totalTimeMinutes;
    }
}
