package org.marcellinus.reporter;

/**
 * @author Eric Eiswerth
 */
public class ReportContext {

    private int total404Pages;

    private int totalInsecurePages;

    private int totalBrokenImagePages;

    private int totalPages;

    private int totalSkippedPages;

    private double totalTimeInMinutes;

    public int getTotal404Pages() {
        return total404Pages;
    }

    public void setTotal404Pages(int total404Pages) {
        this.total404Pages = total404Pages;
    }

    public int getTotalInsecurePages() {
        return totalInsecurePages;
    }

    public void setTotalInsecurePages(int totalInsecurePages) {
        this.totalInsecurePages = totalInsecurePages;
    }

    public int getTotalBrokenImagePages() {
        return totalBrokenImagePages;
    }

    public void setTotalBrokenImagePages(int totalBrokenImagePages) {
        this.totalBrokenImagePages = totalBrokenImagePages;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getTotalSkippedPages() {
        return totalSkippedPages;
    }

    public void setTotalSkippedPages(int totalSkippedPages) {
        this.totalSkippedPages = totalSkippedPages;
    }

    public double getTotalTimeInMinutes() {
        return totalTimeInMinutes;
    }

    public void setTotalTimeInMinutes(double totalTimeInMinutes) {
        this.totalTimeInMinutes = totalTimeInMinutes;
    }
}
