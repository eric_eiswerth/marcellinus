package org.marcellinus.reporter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import net.thucydides.core.ThucydidesSystemProperty;
import net.thucydides.core.reports.html.HtmlResourceCopier;
import net.thucydides.core.util.EnvironmentVariables;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.marcellinus.crawler.CrawlerConstants;
import org.marcellinus.exceptions.ReportException;
import org.marcellinus.reporter.decorators.IReportContextDecorator;
import org.marcellinus.reporter.writers.PageLinkEntityReportContentWriter;
import org.marcellinus.reporter.writers.SkippedLinkReportContentWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Creates the reports.
 *
 * @author Eric Eiswerth
 */
public class Reporter {

    private static final Logger LOGGER = LoggerFactory.getLogger(Reporter.class);

    private static final List<IReportContextDecorator> DECORATORS = new ArrayList<>();

    @Autowired
    private PageLinkEntityReportContentWriter pageLinkEntityReportContentWriter;

    @Autowired
    private SkippedLinkReportContentWriter skippedLinkReportContentWriter;

    @Autowired
    private File outputDir;

    @Autowired
    private EnvironmentVariables environmentVariables;

    /**
     * Generates the crawler reports.
     * @throws ReportException
     */
    public final void report() throws ReportException {
        report(null);
    }

    /**
     * Generates the crawler reports.
     * @param reportDetails
     * @throws ReportException
     */
    public final void report(ReportDetails reportDetails) throws ReportException {
        try {
            copyReportResources();

            final ReportContext reportContext = new ReportContext();
            if (reportDetails != null) {
                reportContext.setTotalTimeInMinutes(reportDetails.getTotalTimeMinutes());
            }

            // The reports must decorate the context BEFORE writing the reports. All reports share the same context.
            pageLinkEntityReportContentWriter.decorateReportContent(reportContext, DECORATORS);
            skippedLinkReportContentWriter.decorateReportContent(reportContext, DECORATORS);

            // Write the reports.
            pageLinkEntityReportContentWriter.writeReportContent(reportContext);
            skippedLinkReportContentWriter.writeReportContent(reportContext);
        } catch (IOException ioe) {
            throw new ReportException("Error writing report.", ioe);
        }
    }

    /**
     * Adds a decorator that can be used to decorate the {@link ReportContext}.
     * @param decorator
     */
    public void addDecorator(IReportContextDecorator decorator) {
        DECORATORS.add(decorator);
    }

    private void copyReportResources() throws IOException {
        LOGGER.info("Copying report resources to: " + outputDir.getAbsolutePath());
        // First, copy the default resources.
        HtmlResourceCopier resourceCopier = new HtmlResourceCopier(CrawlerConstants.REPORT_RESOURCES_DIR);
        resourceCopier.copyHTMLResourcesTo(outputDir);

        // Next, copy the user-defined resources which are free to overwrite the default resources. This is different
        // than what Thucydides does in that you only need to overwrite individual files if you want to brand the tool.
        String customResourceDirectory =
                environmentVariables.getProperty(ThucydidesSystemProperty.REPORT_RESOURCE_PATH.getPropertyName());
        if (StringUtils.isNotEmpty(customResourceDirectory)) {
            final File customResourceFile = new File(customResourceDirectory);
            if (customResourceFile.isDirectory()) {
                FileUtils.copyDirectory(customResourceFile, outputDir);
            }
        }
    }

}
