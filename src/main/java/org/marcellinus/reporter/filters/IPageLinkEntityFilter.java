package org.marcellinus.reporter.filters;

import org.marcellinus.model.PageLinkEntity;

/**
 * @author Eric Eiswerth
 */
public interface IPageLinkEntityFilter {

    public boolean filterPageLinkEntity(PageLinkEntity pageLinkEntity);

}
