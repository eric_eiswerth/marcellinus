package org.marcellinus.reporter.filters;

import org.apache.http.HttpStatus;
import org.marcellinus.model.PageLinkEntity;
import org.springframework.stereotype.Component;

/**
 * @author Eric Eiswerth
 */
@Component("http404Filter")
public class Http404PageLinkEntityFilter implements IPageLinkEntityFilter {

    @Override
    public boolean filterPageLinkEntity(PageLinkEntity pageLinkEntity) {
        return pageLinkEntity.getResponseCode() == HttpStatus.SC_NOT_FOUND;
    }
}
