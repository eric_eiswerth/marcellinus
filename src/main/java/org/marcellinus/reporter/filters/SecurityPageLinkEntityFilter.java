package org.marcellinus.reporter.filters;

import org.marcellinus.model.PageLinkEntity;
import org.springframework.stereotype.Component;

/**
 * @author Eric Eiswerth
 */
@Component("securityFilter")
public class SecurityPageLinkEntityFilter implements IPageLinkEntityFilter {

    @Override
    public boolean filterPageLinkEntity(PageLinkEntity pageLinkEntity) {
        return pageLinkEntity.getUrl().contains("jsessionid");
    }
}
