package org.marcellinus.reporter.filters;

import org.apache.http.HttpStatus;
import org.marcellinus.model.ImageLink;
import org.marcellinus.model.PageLinkEntity;
import org.springframework.stereotype.Component;

/**
 * Filters pages that contain broken images.
 *
 * @author Eric Eiswerth
 */
@Component("brokenImageFilter")
public class BrokenImagePageLinkEntityFilter implements IPageLinkEntityFilter {

    @Override
    public boolean filterPageLinkEntity(PageLinkEntity pageLinkEntity) {
        for (ImageLink imageLink : pageLinkEntity.getImageLinks()) {
            if (imageLink.getResponseCode() == HttpStatus.SC_NOT_FOUND) {
                return true;
            }
        }
        return false;
    }
}
