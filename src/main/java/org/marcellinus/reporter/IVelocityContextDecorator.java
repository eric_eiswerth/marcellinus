package org.marcellinus.reporter;

import org.apache.velocity.VelocityContext;
import org.marcellinus.reporter.reports.IReport;

/**
 * @author Eric Eiswerth
 */
public interface IVelocityContextDecorator {

    public void decorate(VelocityContext velocityContext, ReportContext reportContext, IReport report);

}
