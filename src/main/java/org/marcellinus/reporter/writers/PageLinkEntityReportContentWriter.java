package org.marcellinus.reporter.writers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.marcellinus.exceptions.ReportException;
import org.marcellinus.model.PageLinkEntity;
import org.marcellinus.persistence.exceptions.EntityServiceException;
import org.marcellinus.persistence.service.IEntityService;
import org.marcellinus.reporter.ReportContext;
import org.marcellinus.reporter.decorators.IReportContextDecorator;
import org.marcellinus.reporter.reports.IAggregatePageLinkEntityReport;
import org.marcellinus.reporter.reports.PageReport;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Eric Eiswerth
 */
public class PageLinkEntityReportContentWriter extends AbstractReportContentWriter {

    private final List<IAggregatePageLinkEntityReport> AGGREGATE_REPORTS = new ArrayList<>();

    @Autowired
    private IEntityService<PageLinkEntity> pageLinkEntityService;

    private List<PageLinkEntity> pageLinkEntities;

    @Override
    public void decorateReportContent(ReportContext reportContext, Collection<IReportContextDecorator> decorators)
            throws ReportException {
        try {
            // Report on valid links.
            pageLinkEntities = pageLinkEntityService.list();
            LOGGER.info("Reported on " + pageLinkEntities.size() + " links");

            final List<PageLinkEntity> pageLinkEntitiesCopy = Collections.unmodifiableList(pageLinkEntities);
            for (IReportContextDecorator decorator : decorators) {
                decorator.decorateFromPageLinkEntities(reportContext, pageLinkEntitiesCopy);
            }
        } catch (EntityServiceException ese) {
            throw new ReportException("Error retrieving links.", ese);
        }
    }

    @Override
    public void writeReportContent(ReportContext reportContext)
            throws ReportException {
        try {
            // Sort the results by timestamp.
            Collections.sort(pageLinkEntities);
            generateAggregateReports(pageLinkEntities, reportContext);
            generateSimpleReports(pageLinkEntities, reportContext);
        } catch (IOException ioe) {
            throw new ReportException("Error writing report.", ioe);
        }
    }

    public void addReport(IAggregatePageLinkEntityReport report) {
        AGGREGATE_REPORTS.add(report);
    }

    private void generateAggregateReports(List<PageLinkEntity> pageLinkEntities, ReportContext reportContext)
            throws ReportException, IOException {
        for (IAggregatePageLinkEntityReport report : AGGREGATE_REPORTS) {
            for (PageLinkEntity pageLinkEntity : pageLinkEntities) {
                report.handlePageLinkEntity(pageLinkEntity);
            }
            try {
                getReportWriter().writeReport(report, reportContext);
            } finally {
                report.close();
            }
        }
    }

    private void generateSimpleReports(List<PageLinkEntity> pageLinkEntityList, ReportContext reportContext)
            throws ReportException, IOException {
        for (PageLinkEntity pageLinkEntity : pageLinkEntityList) {
            final PageReport report = new PageReport();
            report.handlePageLinkEntity(pageLinkEntity);
            getReportWriter().writeReport(report, reportContext);
        }
    }

}
