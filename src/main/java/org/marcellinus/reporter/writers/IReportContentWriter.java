package org.marcellinus.reporter.writers;

import java.util.Collection;
import org.marcellinus.exceptions.ReportException;
import org.marcellinus.reporter.ReportContext;
import org.marcellinus.reporter.decorators.IReportContextDecorator;

/**
 * @author Eric Eiswerth
 */
public interface IReportContentWriter {

    public void decorateReportContent(ReportContext reportContext, Collection<IReportContextDecorator> decorators)
            throws ReportException;

    public void writeReportContent(ReportContext reportContext)
            throws ReportException;

}
