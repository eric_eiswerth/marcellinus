package org.marcellinus.reporter.writers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.marcellinus.reporter.IVelocityContextDecorator;
import org.marcellinus.reporter.ReportContext;
import org.marcellinus.reporter.reports.IReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Eric Eiswerth
 */
@Component
public class VelocityReportWriter implements IReportWriter {

    protected static final Logger LOGGER = LoggerFactory.getLogger(VelocityReportWriter.class);

    @Autowired
    private IVelocityContextDecorator velocityContextDecorator;

    @Autowired
    private VelocityEngine velocityEngine;

    @Autowired
    private File outputDir;

    private boolean velocityEngineInitialized = false;

    @Override
    public void writeReport(IReport report, ReportContext reportContext)
            throws IOException {
        if (!velocityEngineInitialized) {
            initVelocity();
            velocityEngineInitialized = true;
        }
        final VelocityContext context = new VelocityContext();
        velocityContextDecorator.decorate(context, reportContext, report);

        Template template = null;
        try {
            template = velocityEngine.getTemplate(report.getTemplateName());
        } catch (ResourceNotFoundException rnfe) {
            LOGGER.error("Unable to create template.", rnfe);
        } catch (ParseErrorException pee) {
            LOGGER.error("Unable to pare template.", pee);
        } catch (MethodInvocationException mie) {
            LOGGER.error("Unable to populate template.", mie);
        }

        if (template != null) {
            final File reportFile = new File(outputDir, report.getReportName());
            BufferedWriter bw = null;
            try {
                bw = new BufferedWriter(new FileWriter(reportFile));
                template.merge(context, bw);
            } finally {
                if (bw != null) {
                    bw.close();
                }
            }
        }
    }

    private void initVelocity() {
        final Properties props = new Properties();
        props.setProperty("resource.loader", "classpath");
        props.setProperty("classpath.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        velocityEngine.init(props);
    }

}
