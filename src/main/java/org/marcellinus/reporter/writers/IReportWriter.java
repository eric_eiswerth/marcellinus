package org.marcellinus.reporter.writers;

import java.io.IOException;
import org.marcellinus.reporter.ReportContext;
import org.marcellinus.reporter.reports.IReport;

/**
 * @author Eric Eiswerth
 */
public interface IReportWriter {

    public void writeReport(IReport report, ReportContext reportContext)
            throws IOException;

}
