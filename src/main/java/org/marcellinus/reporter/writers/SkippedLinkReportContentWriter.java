package org.marcellinus.reporter.writers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.marcellinus.exceptions.ReportException;
import org.marcellinus.model.SkippedLink;
import org.marcellinus.persistence.exceptions.EntityServiceException;
import org.marcellinus.persistence.service.IEntityService;
import org.marcellinus.reporter.ReportContext;
import org.marcellinus.reporter.decorators.IReportContextDecorator;
import org.marcellinus.reporter.reports.ISkippedLinkReport;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Eric Eiswerth
 */
public class SkippedLinkReportContentWriter extends AbstractReportContentWriter {

    private final List<ISkippedLinkReport> SKIPPED_LINK_REPORTS = new ArrayList<ISkippedLinkReport>();

    @Autowired
    private IEntityService<SkippedLink> skippedLinkEntityService;

    private List<SkippedLink> skippedLinks;

    @Override
    public void decorateReportContent(ReportContext reportContext, Collection<IReportContextDecorator> decorators) throws ReportException {
        try {
            // Report on skipped links.
            skippedLinks = skippedLinkEntityService.list();
            LOGGER.info("Reported on " + skippedLinks.size() + " skipped links");

            final List<SkippedLink> skippedLinksCopy = Collections.unmodifiableList(skippedLinks);
            for (IReportContextDecorator decorator : decorators) {
                decorator.decorateFromSkippedLinks(reportContext, skippedLinksCopy);
            }
        } catch (EntityServiceException ese) {
            throw new ReportException("Error retrieving links.", ese);
        }
    }

    @Override
    public void writeReportContent(ReportContext reportContext)
            throws ReportException {
        try {
            Collections.sort(skippedLinks);
            generateSkippedLinkReports(skippedLinks, reportContext);
        } catch (IOException ioe) {
            throw new ReportException("Error writing report.", ioe);
        }
    }

    public void addSkippedLinkReport(ISkippedLinkReport report) {
        SKIPPED_LINK_REPORTS.add(report);
    }

    private void generateSkippedLinkReports(List<SkippedLink> skippedLinks, ReportContext reportContext)
            throws ReportException, IOException {
        for (ISkippedLinkReport report : SKIPPED_LINK_REPORTS) {
            for (SkippedLink skippedLink : skippedLinks) {
                report.handleSkippedLink(skippedLink);
            }
            getReportWriter().writeReport(report, reportContext);
        }
    }
}
