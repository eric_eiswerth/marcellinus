package org.marcellinus.reporter.writers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Eric Eiswerth
 */
public abstract class AbstractReportContentWriter implements IReportContentWriter {

    protected static final Logger LOGGER = LoggerFactory.getLogger(AbstractReportContentWriter.class);

    @Autowired
    private IReportWriter reportWriter;

    public IReportWriter getReportWriter() {
        return reportWriter;
    }

    public void setReportWriter(IReportWriter reportWriter) {
        this.reportWriter = reportWriter;
    }
}
