package org.marcellinus.reporter.decorators;

import java.util.List;
import org.marcellinus.model.PageLinkEntity;
import org.marcellinus.model.SkippedLink;
import org.marcellinus.reporter.ReportContext;
import org.marcellinus.reporter.filters.IPageLinkEntityFilter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Eric Eiswerth
 */
public class ReportContextDecoratorImpl implements IReportContextDecorator {

    @Autowired
    private IPageLinkEntityFilter http404Filter;

    @Autowired
    private IPageLinkEntityFilter securityFilter;

    @Autowired
    private IPageLinkEntityFilter brokenImageFilter;

    @Override
    public void decorateFromPageLinkEntities(ReportContext reportContext, List<PageLinkEntity> pageLinkEntityList) {
        int total404Pages = 0;
        int totalInsecurePages = 0;
        int totalBrokenImagePages = 0;
        int totalPages = 0;
        for (PageLinkEntity pageLinkEntity : pageLinkEntityList) {
            ++totalPages;
            if (http404Filter.filterPageLinkEntity(pageLinkEntity)) {
                ++total404Pages;
            }
            if (securityFilter.filterPageLinkEntity(pageLinkEntity)) {
                ++totalInsecurePages;
            }
            if (brokenImageFilter.filterPageLinkEntity(pageLinkEntity)) {
                ++totalBrokenImagePages;
            }
        }
        reportContext.setTotal404Pages(total404Pages);
        reportContext.setTotalInsecurePages(totalInsecurePages);
        reportContext.setTotalBrokenImagePages(totalBrokenImagePages);
        reportContext.setTotalPages(totalPages);
    }

    @Override
    public void decorateFromSkippedLinks(ReportContext reportContext, List<SkippedLink> skippedLinks) {
        reportContext.setTotalSkippedPages(skippedLinks.size());
    }
}
