package org.marcellinus.reporter.decorators;

import java.util.List;
import org.marcellinus.model.PageLinkEntity;
import org.marcellinus.model.SkippedLink;
import org.marcellinus.reporter.ReportContext;

/**
 * @author Eric Eiswerth
 */
public interface IReportContextDecorator {

    public void decorateFromPageLinkEntities(ReportContext reportContext, List<PageLinkEntity> pageLinkEntityList);

    public void decorateFromSkippedLinks(ReportContext reportContext, List<SkippedLink> skippedLinks);

}
