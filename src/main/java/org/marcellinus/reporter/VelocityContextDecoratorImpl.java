package org.marcellinus.reporter;

import org.apache.velocity.VelocityContext;
import org.marcellinus.conf.ICrawlerConfiguration;
import org.marcellinus.reporter.reports.IReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Eric Eiswerth
 */
@Component("velocityContextDecorator")
public class VelocityContextDecoratorImpl implements IVelocityContextDecorator {

    @Autowired
    private ICrawlerConfiguration configuration;

    @Override
    public void decorate(VelocityContext velocityContext, ReportContext reportContext, IReport report) {
        velocityContext.put("baseUrl", configuration.getBaseUrl());
        velocityContext.put("total404Pages", reportContext.getTotal404Pages());
        velocityContext.put("totalInsecurePages", reportContext.getTotalInsecurePages());
        velocityContext.put("totalBrokenImagePages", reportContext.getTotalBrokenImagePages());
        velocityContext.put("totalPages", reportContext.getTotalPages());
        velocityContext.put("totalSkippedPages", reportContext.getTotalSkippedPages());
        velocityContext.put("totalTimeInMinutes", reportContext.getTotalTimeInMinutes());
        velocityContext.put("pageTitle", report.getPageTitle());
        velocityContext.put("versionNumber", configuration.getBuildVersion());
        report.populateContext(velocityContext);
    }
}
