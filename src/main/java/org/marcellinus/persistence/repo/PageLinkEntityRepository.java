package org.marcellinus.persistence.repo;

import org.marcellinus.model.PageLinkEntity;
import org.springframework.stereotype.Repository;

/**
 * @author Eric Eiswerth
 */
@Repository("pageLinkEntityRepository")
public class PageLinkEntityRepository extends BaseEntityRepository<PageLinkEntity> {

    @Override
    protected Class getStoredClassType() {
        return PageLinkEntity.class;
    }
}
