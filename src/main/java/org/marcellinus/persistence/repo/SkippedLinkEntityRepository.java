package org.marcellinus.persistence.repo;

import org.marcellinus.model.SkippedLink;
import org.springframework.stereotype.Repository;

/**
 * @author Eric Eiswerth
 */
@Repository("skippedLinkEntityRepository")
public class SkippedLinkEntityRepository extends BaseEntityRepository<SkippedLink> {

    @Override
    protected Class getStoredClassType() {
        return SkippedLink.class;
    }
}
