package org.marcellinus.persistence.repo;

import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Transaction;
import com.sleepycat.persist.EntityCursor;
import com.sleepycat.persist.PrimaryIndex;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import net.thucydides.core.webdriver.Configuration;
import org.marcellinus.model.LinkID;
import org.marcellinus.persistence.DbEnv;
import org.marcellinus.persistence.exceptions.EntityRepositoryException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Eric Eiswerth
 * @param <T>
 */
public abstract class BaseEntityRepository<T> implements IEntityRepository<T> {

    @Autowired
    private DbEnv dbEnv;

    @Autowired
    private Configuration configuration;

    @Autowired
    private File dbDir;

    @Override
    public final void create(T t) throws EntityRepositoryException {
        try {
            setupDb();

            final PrimaryIndex<LinkID, T> entityByID = dbEnv.getStore().getPrimaryIndex(LinkID.class, getStoredClassType());
            final Transaction txn = dbEnv.getEnvironment().beginTransaction(null, null);
            try {
                entityByID.put(txn, t);
            } finally {
                txn.commit();
            }
        } catch(DatabaseException de) {
            throw new EntityRepositoryException("Error creating entity.", de);
        } finally {
            dbEnv.close();
        }
    }

    @Override
    public final T read(LinkID linkID) throws EntityRepositoryException {
        T result = null;
        try {
            setupDb();

            final PrimaryIndex<LinkID, T> entityByID = dbEnv.getStore().getPrimaryIndex(LinkID.class, getStoredClassType());
            final Transaction txn = dbEnv.getEnvironment().beginTransaction(null, null);
            try {
                result = entityByID.get(txn, linkID, null);
            } finally {
                txn.commit();
            }
        } catch(DatabaseException de) {
            throw new EntityRepositoryException("Error getting entity.", de);
        } finally {
            dbEnv.close();
        }
        return result;
    }

    @Override
    public final List<T> list() throws EntityRepositoryException {
        final List<T> results = new ArrayList<>();
        try {
            setupDb();

            final PrimaryIndex<LinkID, T> entityByID = dbEnv.getStore().getPrimaryIndex(LinkID.class, getStoredClassType());
            final EntityCursor<T> entityCursor = entityByID.entities();
            try {
                for (T t : entityCursor) {
                    results.add(t);
                }
            } finally {
                entityCursor.close();
            }
        } catch(DatabaseException de) {
            throw new EntityRepositoryException("Error retrieving all entities.", de);
        } finally {
            dbEnv.close();
        }
        return results;
    }

    private void setupDb() throws DatabaseException {
        dbEnv.setup(dbDir, false);
    }

    protected DbEnv getDbEnv() {
        return dbEnv;
    }

    protected abstract Class getStoredClassType();

}
