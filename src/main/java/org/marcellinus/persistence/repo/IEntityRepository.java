package org.marcellinus.persistence.repo;

import java.util.List;
import org.marcellinus.model.LinkID;
import org.marcellinus.persistence.exceptions.EntityRepositoryException;

/**
 * @author Eric Eiswerth
 * @param <T>
 */
public interface IEntityRepository<T> {

    /**
     * Creates a new entity.
     * @param t
     * @throws EntityRepositoryException
     */
    public void create(T t) throws EntityRepositoryException;

    /**
     * Retrieves an entity by ID.
     * @param linkID
     * @return
     * @throws EntityRepositoryException
     */
    public T read(LinkID linkID) throws EntityRepositoryException;

    /**
     * Retrieves all entities.
     * @return
     * @throws EntityRepositoryException
     */
    public List<T> list() throws EntityRepositoryException;

}
