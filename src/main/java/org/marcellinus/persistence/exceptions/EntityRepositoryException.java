package org.marcellinus.persistence.exceptions;

/**
 * @author Eric Eiswerth
 */
public class EntityRepositoryException extends Exception {

    public EntityRepositoryException(String message, Throwable cause) {
        super(message, cause);
    }

}
