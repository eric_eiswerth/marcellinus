package org.marcellinus.persistence.exceptions;

/**
 * @author Eric Eiswerth
 */
public class EntityServiceException extends Exception {

    public EntityServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public EntityServiceException(Throwable cause) {
        super(cause);
    }

}
