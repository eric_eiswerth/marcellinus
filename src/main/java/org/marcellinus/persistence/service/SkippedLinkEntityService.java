package org.marcellinus.persistence.service;

import java.util.List;
import org.marcellinus.model.LinkID;
import org.marcellinus.model.SkippedLink;
import org.marcellinus.persistence.exceptions.EntityRepositoryException;
import org.marcellinus.persistence.exceptions.EntityServiceException;
import org.marcellinus.persistence.repo.SkippedLinkEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Eric Eiswerth
 */
@Service("skippedLinkEntityService")
public class SkippedLinkEntityService implements IEntityService<SkippedLink> {

    @Autowired
    private SkippedLinkEntityRepository skippedLinkIEntityRepository;

    @Override
    public void create(SkippedLink skippedLink) throws EntityServiceException {
        try {
            skippedLinkIEntityRepository.create(skippedLink);
        } catch (EntityRepositoryException ere) {
            throw new EntityServiceException(ere);
        }
    }

    @Override
    public SkippedLink read(LinkID linkID) throws EntityServiceException {
        try {
            return skippedLinkIEntityRepository.read(linkID);
        } catch (EntityRepositoryException ere) {
            throw new EntityServiceException(ere);
        }
    }

    @Override
    public List<SkippedLink> list() throws EntityServiceException {
        try {
            return skippedLinkIEntityRepository.list();
        } catch (EntityRepositoryException ere) {
            throw new EntityServiceException(ere);
        }
    }
}
