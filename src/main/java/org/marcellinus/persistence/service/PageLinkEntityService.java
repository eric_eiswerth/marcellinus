package org.marcellinus.persistence.service;

import java.util.List;
import org.marcellinus.model.LinkID;
import org.marcellinus.model.PageLinkEntity;
import org.marcellinus.persistence.exceptions.EntityRepositoryException;
import org.marcellinus.persistence.exceptions.EntityServiceException;
import org.marcellinus.persistence.repo.PageLinkEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Eric Eiswerth
 */
@Service("pageLinkEntityService")
public class PageLinkEntityService implements IEntityService<PageLinkEntity> {

    @Autowired
    private PageLinkEntityRepository pageLinkEntityRepository;

    @Override
    public void create(PageLinkEntity pageLinkEntity) throws EntityServiceException {
        try {
            pageLinkEntityRepository.create(pageLinkEntity);
        } catch (EntityRepositoryException ere) {
            throw new EntityServiceException(ere);
        }
    }

    @Override
    public PageLinkEntity read(LinkID linkID) throws EntityServiceException {
        try {
            return pageLinkEntityRepository.read(linkID);
        } catch (EntityRepositoryException ere) {
            throw new EntityServiceException(ere);
        }
    }

    @Override
    public List<PageLinkEntity> list() throws EntityServiceException {
        try {
            return pageLinkEntityRepository.list();
        } catch (EntityRepositoryException ere) {
            throw new EntityServiceException(ere);
        }
    }
}
