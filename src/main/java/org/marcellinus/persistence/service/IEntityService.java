package org.marcellinus.persistence.service;

import java.util.List;
import org.marcellinus.model.LinkID;
import org.marcellinus.persistence.exceptions.EntityServiceException;

/**
 * @author Eric Eiswerth
 * @param <T>
 */
public interface IEntityService<T> {

    /**
     * Creates a new entity.
     * @param t
     * @throws EntityServiceException
     */
    public void create(T t) throws EntityServiceException;

    /**
     * Retrieves an entity by ID.
     * @param linkID
     * @return
     * @throws EntityServiceException
     */
    public T read(LinkID linkID) throws EntityServiceException;

    /**
     * Retrieves all entities.
     * @return
     * @throws EntityServiceException
     */
    public List<T> list() throws EntityServiceException;

}
