package org.marcellinus.persistence;

import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.StoreConfig;
import java.io.File;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component("dbEnv")
public class DbEnv {

    private static final Logger LOGGER = LoggerFactory.getLogger(DbEnv.class);

    private Environment environment;

    private EntityStore store;

    /**
     * This is responsible for instantiating our Environment object. Remember that instantiation is what opens the
     * environment (or creates it if the creation property is set to true and the environment does not currently exist)
     * @param envHome
     * @param readOnly
     * @throws DatabaseException
     */
    public void setup(File envHome, boolean readOnly)
            throws DatabaseException {
        envHome.mkdirs();
        // Instantiate an environment and database configuration object
        EnvironmentConfig envConfig = new EnvironmentConfig();
        envConfig.setAllowCreate(true);
        envConfig.setTransactional(true);

        // Instantiate the Environment. This opens it and also possibly
        // creates it.
        environment = new Environment(envHome, envConfig);

        StoreConfig storeConfig = new StoreConfig();
        storeConfig.setAllowCreate(true);
        storeConfig.setTransactional(true);
        store = new EntityStore(environment, "MyStore", storeConfig);
    }

    /**
     * Closes the DB.
     */
    public void close() {
        if (environment != null) {
            try {
                store.close();
                environment.close();
            } catch(DatabaseException dbe) {
                LOGGER.error("Error closing database.", dbe);
            }
        }
    }

    public EntityStore getStore() {
        return store;
    }

    public Environment getEnvironment() {
        return environment;
    }

}
