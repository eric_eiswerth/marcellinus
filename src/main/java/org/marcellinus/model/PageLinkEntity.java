package org.marcellinus.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;
import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;

/**
 * Use the {@link LinkFactory} to instantiate.
 *
 * @author Eric Eiswerth
 */
@Entity
public class PageLinkEntity extends LinkEntity implements Serializable {

    @PrimaryKey
    private LinkID linkID;

    @JsonProperty("image")
    private String screenshotPath;

    @JsonProperty("thumb")
    private String screenshotThumbPath;

    private int responseCode;

    @JsonProperty("description")
    private String description;

    @JsonProperty("big")
    private String big;

    private Set<LinkEntity> incomingLinks = new TreeSet<>();

    private Set<LinkEntity> outgoingLinks = new TreeSet<>();

    private String redirectedFromUrl;

    private Set<ImageLink> imageLinks = new TreeSet<>();

    private String pageSource;

    private boolean artifact;

    PageLinkEntity() {
    }

    public LinkID getLinkID() {
        return linkID;
    }

    public void setLinkID(LinkID linkID) {
        this.linkID = linkID;
    }

    public void setUrl(String url) {
        super.setUrl(url);
        this.description = url;
        this.big = url;
    }

    public String getScreenshotThumbPath() {
        return screenshotThumbPath;
    }

    public void setScreenshotThumbPath(String screenshotThumbPath) {
        this.screenshotThumbPath = screenshotThumbPath;
    }

    public String getScreenshotPath() {
        return screenshotPath;
    }

    public void setScreenshotPath(String screenshotPath) {
        this.screenshotPath = screenshotPath;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBig() {
        return big;
    }

    public Set<LinkEntity> getIncomingLinks() {
        return incomingLinks;
    }

    public void addIncomingLink(LinkEntity linkEntity) {
        if (linkEntity != null) {
            incomingLinks.add(linkEntity);
        }
    }

    public Set<LinkEntity> getOutgoingLinks() {
        return outgoingLinks;
    }

    public void addOutgoingLink(LinkEntity linkEntity) {
        if (linkEntity != null) {
            outgoingLinks.add(linkEntity);
        }
    }

    public String getRedirectedFromUrl() {
        return redirectedFromUrl;
    }

    public void setRedirectedFromUrl(String redirectedFromUrl) {
        this.redirectedFromUrl = redirectedFromUrl;
    }

    public boolean isRedirected() {
        return redirectedFromUrl != null;
    }

    public Set<ImageLink> getImageLinks() {
        return imageLinks;
    }

    public void addImageLink(ImageLink link) {
        if (link != null) {
            imageLinks.add(link);
        }
    }

    public String getPageSource() {
        return pageSource;
    }

    public void setPageSource(String pageSource) {
        this.pageSource = pageSource;
    }

    public boolean isArtifact() {
        return artifact;
    }

    public void setArtifact(boolean artifact) {
        this.artifact = artifact;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof PageLinkEntity)) {
            return false;
        }
        final PageLinkEntity linkEntity = (PageLinkEntity)obj;
        return getLinkID().equals(linkEntity.getLinkID());
    }

    @Override
    public int hashCode() {
        return getLinkID().hashCode();
    }

    @Override
    public String toString() {
        return getLinkID().toString();
    }

}
