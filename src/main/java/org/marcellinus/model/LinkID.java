package org.marcellinus.model;

import com.sleepycat.persist.model.KeyField;
import com.sleepycat.persist.model.Persistent;
import java.io.Serializable;

/**
 * Use the {@link LinkFactory} to instantiate.
 *
 * @author Eric Eiswerth
 */
@Persistent
public class LinkID implements Serializable {

    @KeyField(1)
    private String key;

    /**
     * Default constructor. Required for persistence.
     */
    LinkID() {
    }

    LinkID(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof LinkID)) {
            return false;
        }
        final LinkID uniqueID = (LinkID)obj;
        return getKey().equals(uniqueID.getKey());
    }

    @Override
    public int hashCode() {
        return getKey().hashCode();
    }

    @Override
    public String toString() {
        return key;
    }
}
