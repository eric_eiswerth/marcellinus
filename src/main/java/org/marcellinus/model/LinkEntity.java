package org.marcellinus.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sleepycat.persist.model.Persistent;
import java.io.Serializable;

/**
 * Use the {@link LinkFactory} to instantiate.
 *
 * @author Eric Eiswerth
 */
@Persistent
public class LinkEntity implements Serializable, Comparable<LinkEntity> {

    @JsonProperty("key")
    private String key;

    @JsonProperty("link")
    private String link;

    @JsonProperty("title")
    private String url;

    private long timestamp;

    private String rawKey;

    LinkEntity() {
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getRawKey() {
        return rawKey;
    }

    public void setRawKey(String rawKey) {
        this.rawKey = rawKey;
    }

    @Override
    public int compareTo(LinkEntity o) {
        return Long.valueOf(getTimestamp()).compareTo(o.getTimestamp());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof LinkEntity)) {
            return false;
        }
        final LinkEntity linkEntity = (LinkEntity)obj;
        return getKey().equals(linkEntity.getKey());
    }

    @Override
    public int hashCode() {
        return getKey().hashCode();
    }

    @Override
    public String toString() {
        return getKey().toString();
    }
}
