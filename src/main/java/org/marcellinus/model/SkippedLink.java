package org.marcellinus.model;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;
import java.io.Serializable;

/**
 * Use the {@link LinkFactory} to instantiate.
 *
 * @author Eric Eiswerth
 */
@Entity
public class SkippedLink implements Serializable, Comparable<SkippedLink> {

    @PrimaryKey
    private LinkID linkID;

    private String url;

    SkippedLink() {
    }

    public LinkID getLinkID() {
        return linkID;
    }

    public void setLinkID(LinkID linkID) {
        this.linkID = linkID;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int compareTo(SkippedLink skippedLink) {
        return getUrl().compareTo(skippedLink.getUrl());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof SkippedLink)) {
            return false;
        }
        final SkippedLink skippedLink = (SkippedLink)obj;
        return getLinkID().equals(skippedLink.getLinkID());
    }

    @Override
    public int hashCode() {
        return getLinkID().hashCode();
    }

    @Override
    public String toString() {
        return getLinkID().toString();
    }
}
