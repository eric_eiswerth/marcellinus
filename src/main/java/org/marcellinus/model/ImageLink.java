package org.marcellinus.model;

import com.sleepycat.persist.model.Persistent;
import java.io.Serializable;

/**
 * Use the {@link LinkFactory} to instantiate.
 *
 * @author Eric Eiswerth
 */
@Persistent
public class ImageLink implements Serializable, Comparable<ImageLink> {

    private String url;

    private int responseCode;

    ImageLink() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    @Override
    public int compareTo(ImageLink imageLink) {
        return getUrl().compareTo(imageLink.getUrl());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof  ImageLink)) {
            return false;
        }
        final ImageLink imageLink = (ImageLink)obj;
        return getUrl().equals(imageLink.getUrl());
    }

    @Override
    public int hashCode() {
        return url.hashCode();
    }

    @Override
    public String toString() {
        return url;
    }
}
