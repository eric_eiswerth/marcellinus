package org.marcellinus.model;

import java.util.Objects;
import org.marcellinus.utils.CrawlerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Eric Eiswerth
 */
@Component("linkFactory")
public class LinkFactory {

    @Autowired
    private CrawlerUtils crawlerUtils;

    public LinkEntity getLinkEntity(PageLinkEntity pageLinkEntity) {
        Objects.requireNonNull(pageLinkEntity, "Link entity cannot be null.");

        final LinkEntity linkEntity = new LinkEntity();
        linkEntity.setKey(pageLinkEntity.getKey());
        linkEntity.setRawKey(pageLinkEntity.getRawKey());
        linkEntity.setTimestamp(pageLinkEntity.getTimestamp());
        linkEntity.setUrl(pageLinkEntity.getUrl());
        linkEntity.setLink(pageLinkEntity.getLinkID() + ".html");
        return linkEntity;
    }

    public PageLinkEntity getPageLinkEntity(String url, String originalUrl, boolean artifact) {
        Objects.requireNonNull(url, "URL cannot be null.");
        Objects.requireNonNull(originalUrl, "Original URL cannot be null.");
        final PageLinkEntity pageLinkEntity = new PageLinkEntity();
        final LinkID linkID = getLinkID(originalUrl);
        pageLinkEntity.setLinkID(linkID);
        pageLinkEntity.setKey(linkID.getKey());
        pageLinkEntity.setRawKey(originalUrl);
        pageLinkEntity.setUrl(url);
        pageLinkEntity.setLink(linkID + ".html");
        pageLinkEntity.setTimestamp(System.currentTimeMillis());
        pageLinkEntity.setArtifact(artifact);
        if (!crawlerUtils.equalsUrl(originalUrl, url)) {
            pageLinkEntity.setRedirectedFromUrl(originalUrl);
        }
        return pageLinkEntity;
    }

    public LinkID getLinkID(String url) {
        return new LinkID(crawlerUtils.generateUniqueKeyFromUrl(url));
    }

    public ImageLink getImageLink(String url, int responseCode) {
        final ImageLink imageLink = new ImageLink();
        imageLink.setUrl(url);
        imageLink.setResponseCode(responseCode);
        return imageLink;
    }

    public SkippedLink getSkippedLink(String url) {
        final SkippedLink skippedLink = new SkippedLink();
        skippedLink.setUrl(url);
        skippedLink.setLinkID(getLinkID(url));
        return skippedLink;
    }

}
