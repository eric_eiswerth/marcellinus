package org.marcellinus.conf;

import net.thucydides.core.webdriver.Configuration;

/**
 * @author Eric Eiswerth
 */
public interface ICrawlerConfiguration extends Configuration {

    /**
     * Gets the max number of unique links to crawl.
     * @return
     */
    public int getMaxLinksToCrawl();

    /**
     * Gets the number of milliseconds to wait for a page to redirect. The default is -1, which is interpreted as do not
     * wait.
     * @return
     */
    public int getRedirectWaitTime();

    /**
     * Gets the pat of the config file.
     * @return
     */
    public String getConfigFileLocation();

    /**
     * Gets the build version.
     * @return
     */
    public String getBuildVersion();

    /**
     * <p>Gets whether or not hashes in URLs should be supported. If they are supported, then they will not be stripped
     * from the URL for canonicalization. URL hashes should be supported for AJAX applications because the hash is
     * typically used for controlling how a page is rendered. This is in contrast to named anchors which use hashes
     * to jump to different parts of the page. In the latter case, we can safely strip hashes (i.e., they are not
     * supported) because we are essentially viewing the same page.</p>
     *
     * <p>By default hashes are not supported. That is, hashes are treated as if they are used ONLY for named anchors.</p>
     * @return
     */
    public boolean isHashSupported();

}
