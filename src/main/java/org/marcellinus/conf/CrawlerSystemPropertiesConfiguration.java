package org.marcellinus.conf;

import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.webdriver.SystemPropertiesConfiguration;
import org.springframework.beans.factory.annotation.Value;

/**
 * @author Eric Eiswerth
 */
public class CrawlerSystemPropertiesConfiguration extends SystemPropertiesConfiguration implements ICrawlerConfiguration {

    @Value("${build.version.number}")
    private String buildVersion;

    public CrawlerSystemPropertiesConfiguration(EnvironmentVariables environmentVariables) {
        super(environmentVariables);
    }

    @Override
    public int getMaxLinksToCrawl() {
        return getEnvironmentVariables().getPropertyAsInteger(CrawlerSystemProperty.MAX_LINKS.getPropertyName(), -1);
    }

    @Override
    public int getRedirectWaitTime() {
        return getEnvironmentVariables().getPropertyAsInteger(CrawlerSystemProperty.REDIRECT_WAIT_TIME.getPropertyName(), -1);
    }

    @Override
    public String getConfigFileLocation() {
        return getEnvironmentVariables().getProperty(CrawlerSystemProperty.CONFIG_FILE_LOCATION.getPropertyName());
    }

    @Override
    public String getBuildVersion() {
        return buildVersion;
    }

    @Override
    public boolean isHashSupported() {
        return getEnvironmentVariables().getPropertyAsBoolean(CrawlerSystemProperty.URL_HASH_SUPPORT.getPropertyName(), Boolean.FALSE);
    }
}
