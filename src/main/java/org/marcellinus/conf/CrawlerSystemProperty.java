package org.marcellinus.conf;

/**
 * @author Eric Eiswerth
 */
public enum CrawlerSystemProperty  {

    MAX_LINKS("crawler.max.links"),
    REDIRECT_WAIT_TIME("crawler.redirect.wait"),
    CONFIG_FILE_LOCATION("crawler.config.file"),
    URL_HASH_SUPPORT("crawler.hash.support");

    private final String propertyName;

    private CrawlerSystemProperty(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPropertyName() {
        return propertyName;
    }

}
