package org.marcellinus.conf.spring;

import org.marcellinus.messages.handlers.SkippedLinkDBMessageHandler;
import org.marcellinus.messages.handlers.SkippedLinkLoggerMessageHandler;
import org.marcellinus.model.SkippedLink;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.PublishSubscribeChannel;

/**
 * Spring integration configuration.
 *
 * @author Eric Eiswerth
 */
@Configuration
public class IntegrationConfig {

    @Bean(name = "messageChannel")
    public PublishSubscribeChannel getMessageChannel() {
        final PublishSubscribeChannel messageChannel = new PublishSubscribeChannel();
        messageChannel.setDatatypes(SkippedLink.class);
        messageChannel.subscribe(getSkippedLinkLoggerMessageHandler());
        messageChannel.subscribe(getSkippedLinkDBMessageHandler());
        return messageChannel;
    }

    @Bean(name = "skippedLinkLoggerMessageHandler")
    public SkippedLinkLoggerMessageHandler getSkippedLinkLoggerMessageHandler() {
        return new SkippedLinkLoggerMessageHandler();
    }

    @Bean(name = "skippedLinkDBMessageHandler")
    public SkippedLinkDBMessageHandler getSkippedLinkDBMessageHandler() {
        return new SkippedLinkDBMessageHandler();
    }

}
