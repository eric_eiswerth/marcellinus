package org.marcellinus.conf.spring;

import java.io.File;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;
import net.thucydides.core.webdriver.ThucydidesWebdriverManager;
import net.thucydides.core.webdriver.WebDriverFactory;
import net.thucydides.core.webdriver.WebdriverManager;
import org.apache.velocity.app.VelocityEngine;
import org.marcellinus.App;
import org.marcellinus.conf.CrawlerSystemPropertiesConfiguration;
import org.marcellinus.conf.ICrawlerConfiguration;
import org.marcellinus.conf.IUserConfiguration;
import org.marcellinus.conf.XMLUserConfiguration;
import org.marcellinus.crawler.Crawler;
import org.marcellinus.crawler.LinkValidator;
import org.marcellinus.crawler.interceptors.*;
import org.marcellinus.reporter.Reporter;
import org.marcellinus.reporter.decorators.IReportContextDecorator;
import org.marcellinus.reporter.decorators.ReportContextDecoratorImpl;
import org.marcellinus.reporter.reports.*;
import org.marcellinus.reporter.writers.PageLinkEntityReportContentWriter;
import org.marcellinus.reporter.writers.SkippedLinkReportContentWriter;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

/**
 * Spring configuration for the application.
 *
 * @author Eric Eiswerth
 */
@org.springframework.context.annotation.Configuration
@ComponentScan({"org.marcellinus"})
@Import({IntegrationConfig.class})
public class AppConfig {

    @Bean(name = "app")
    public App getApp() {
        return new App();
    }

    @Bean(name = "propertyPlaceholderConfigurer")
    public static PropertyPlaceholderConfigurer getPropertyPlaceholderConfigurer() {
        final PropertyPlaceholderConfigurer placeholderConfigurer = new PropertyPlaceholderConfigurer();
        Resource[] locations = {
                new ClassPathResource("build.properties")
        };
        placeholderConfigurer.setLocations(locations);
        placeholderConfigurer.setIgnoreUnresolvablePlaceholders(true);
        return placeholderConfigurer;
    }

    @Bean(name = "environmentVariables")
    public EnvironmentVariables getEnvironmentVariables() {
        return new SystemEnvironmentVariables();
    }

    @Bean(name = "configuration")
    public ICrawlerConfiguration getConfiguration() {
        return new CrawlerSystemPropertiesConfiguration(getEnvironmentVariables());
    }

    @Bean(name = "webDriverFactory")
    public WebDriverFactory getWebDriverFactory() {
        return new WebDriverFactory(getEnvironmentVariables());
    }

    @Bean(name = "webDriverManager")
    public WebdriverManager getWebDriverManager() {
        return new ThucydidesWebdriverManager(getWebDriverFactory(), getConfiguration());
    }

    /**
     * Configures the link validator for validating hyperlinks found on the site.
     * @return
     */
    @Bean(name = "linkValidator")
    public LinkValidator getLinkValidator() {
        final LinkValidator linkValidator = new LinkValidator(getConfiguration().getBaseUrl());
        final IUserConfiguration userConfiguration = getUserConfiguration();
        if (userConfiguration != null) {
            for (String value : userConfiguration.getInvalidUrlContents()) {
                linkValidator.addInvalidUrlContent(value);
            }
            for (String value : userConfiguration.getSkippedExtensions()) {
                linkValidator.addSkippedExtension(value);
            }
            for (String value : userConfiguration.getSkippedUrls()) {
                linkValidator.addSkippedUrl(value);
            }
        }
        return linkValidator;
    }

    @Bean(name = "screenshotLinkInterceptor")
    public ILinkInterceptor getScreenshotLinkInterceptor() {
        return new ScreenshotLinkInterceptor();
    }

    @Bean(name = "httpResponseCodeLinkInterceptor")
    public ILinkInterceptor getHTTPResponseCodeLinkInterceptor() {
        return new HTTPResponseCodeInterceptor();
    }

    @Bean(name = "imageLinkInterceptor")
    public ILinkInterceptor getImageLinkInterceptor() {
        return new ImageLinkInterceptor();
    }

    @Bean(name = "domLinkInterceptor")
    public ILinkInterceptor getDOMInterceptor() {
        return new DOMLinkInterceptor();
    }

    @Bean(name = "crawler")
    public Crawler getCrawler() {
        final Crawler crawler = new Crawler();
        crawler.addLinkInterceptor(getScreenshotLinkInterceptor());
        crawler.addLinkInterceptor(getHTTPResponseCodeLinkInterceptor());
        crawler.addLinkInterceptor(getImageLinkInterceptor());
        crawler.addLinkInterceptor(getDOMInterceptor());
        return crawler;
    }

    @Bean(name = "mainReport")
    public IAggregatePageLinkEntityReport getMainReport() {
        return new MainReport();
    }

    @Bean(name = "http404Report")
    public IAggregatePageLinkEntityReport getHttp404Report() {
        return new Http404Report();
    }

    @Bean(name = "securityReport")
    public IAggregatePageLinkEntityReport getSecurityReport() {
        return new SecurityReport();
    }

    @Bean(name = "allLinksReport")
    public IAggregatePageLinkEntityReport getAllLinksReport() {
        return new AllLinksReport();
    }

    @Bean(name = "brokenImagesReport")
    public IAggregatePageLinkEntityReport getBrokenImagesReport() {
        return new BrokenImagesReport();
    }

    @Bean(name = "graphReport")
    public IAggregatePageLinkEntityReport getGraphReport() {
        return new GraphReport();
    }

    @Bean(name = "reportContextDecorator")
    public IReportContextDecorator getReportContextDecorator() {
        return new ReportContextDecoratorImpl();
    }

    @Bean(name = "pageLinkEntityReportContentWriter")
    public PageLinkEntityReportContentWriter getPageLinkEntityReportContentWriter() {
        final PageLinkEntityReportContentWriter pageLinkEntityReportContentWriter = new PageLinkEntityReportContentWriter();
        pageLinkEntityReportContentWriter.addReport(getMainReport());
        pageLinkEntityReportContentWriter.addReport(getHttp404Report());
        pageLinkEntityReportContentWriter.addReport(getSecurityReport());
        pageLinkEntityReportContentWriter.addReport(getBrokenImagesReport());
        pageLinkEntityReportContentWriter.addReport(getAllLinksReport());
        //pageLinkEntityReportContentWriter.addReport(getGraphReport());
        return pageLinkEntityReportContentWriter;
    }

    @Bean(name = "skippedLinkReport")
    public ISkippedLinkReport getSkippedLinkReport() {
        return new DefaultSkippedLinkReport();
    }

    @Bean(name = "skippedLinkReportContentWriter")
    public SkippedLinkReportContentWriter getSkippedLinkReportWriter() {
        final SkippedLinkReportContentWriter skippedLinkReportContentWriter = new SkippedLinkReportContentWriter();
        skippedLinkReportContentWriter.addSkippedLinkReport(getSkippedLinkReport());
        return skippedLinkReportContentWriter;
    }

    @Bean(name = "reporter")
    public Reporter getReporter() {
        final Reporter reporter = new Reporter();
        reporter.addDecorator(getReportContextDecorator());
        return reporter;
    }

    @Bean(name = "velocityEngine")
    public VelocityEngine getVelocityEngine() {
        return new VelocityEngine();
    }

    @Bean(name = "userConfiguration")
    public IUserConfiguration getUserConfiguration() {
        final IUserConfiguration userConfiguration = new XMLUserConfiguration();
        try {
            userConfiguration.load(getConfiguration());
        } catch (Exception e) {
            throw new RuntimeException("Error while loading configuration.", e);
        }
        return userConfiguration;
    }

    @Bean(name = "outputDir")
    public File getOutputDir() {
        return getConfiguration().getOutputDirectory();
    }

    @Bean(name = "dbDir")
    public File getDbDir() {
        return new File(getOutputDir(), "JEDB");
    }

    @Bean(name = "screenshotsDir")
    public File getScreenshotsDir() {
        return new File(getOutputDir(), "crawlershots");
    }

}
