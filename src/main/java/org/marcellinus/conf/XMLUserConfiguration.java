package org.marcellinus.conf;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;
import org.marcellinus.crawler.endtasks.IEndTask;
import org.marcellinus.crawler.interceptors.ILinkInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default user configuration provider. It supports user configuration via crawler.xml. The location of the crawler.xml
 * file can be provided via the crawler.config.file system property or it can be placed on the classpath.
 *
 * @author Eric Eiswerth
 */
public class XMLUserConfiguration implements IUserConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(XMLUserConfiguration.class);

    private final List<ILinkInterceptor> linkInterceptors = new ArrayList<>();

    private final List<IEndTask> endTasks = new ArrayList<>();

    private final List<String> invalidUrlContents = new ArrayList<>();

    private final List<String> skippedExtensionContents = new ArrayList<>();

    private final List<String> skippedUrlContents = new ArrayList<>();

    private XMLConfiguration config;

    @Override
    public void load(ICrawlerConfiguration configuration) throws ConfigurationException {
        Objects.requireNonNull(configuration, "Configuration cannot be null.");
        try {
            loadConfigFile(configuration);
            if (config != null) {
                initLinkInterceptors();
                initEndTasks();
                initInvalidUrlContents();
                initSkippedExtensions();
                initSkippedUrls();
            }
        } catch (ClassNotFoundException cnfe) {
            throw new ConfigurationException("Error loading interceptors.", cnfe);
        } catch (InstantiationException ie) {
            throw new ConfigurationException("Error loading interceptors.", ie);
        } catch (IllegalAccessException iae) {
            throw new ConfigurationException("Error loading interceptors.", iae);
        }
    }

    @Override
    public List<ILinkInterceptor> getLinkInterceptors() {
        return linkInterceptors;
    }

    @Override
    public List<IEndTask> getEndTasks() {
        return endTasks;
    }

    @Override
    public List<String> getInvalidUrlContents() {
        return invalidUrlContents;
    }

    @Override
    public List<String> getSkippedExtensions() {
        return skippedExtensionContents;
    }

    @Override
    public List<String> getSkippedUrls() {
        return skippedUrlContents;
    }

    private void loadConfigFile(ICrawlerConfiguration configuration) throws ConfigurationException {
        final String configFileLocation = configuration.getConfigFileLocation();
        if (configFileLocation != null) {
            final File configFile = new File(configFileLocation);
            if (!configFile.isFile()) {
                throw new ConfigurationException("Invalid configuration file: " + configFileLocation);
            }
            LOGGER.info("Attempting to load crawler configuration from: " + configFileLocation);
            config = new XMLConfiguration(configFile);
        } else {
            LOGGER.info("Attempting to load crawler.xml from classpath.");
            try {
                config = new XMLConfiguration("crawler.xml");
            } catch (Exception e) {
                // If the configuration couldn't load the crawler.xml then the user probably didn't specify one. Log it
                // and continue.
                config = null;
            }
        }
        if (config == null) {
            LOGGER.warn("Unable to load crawler configuration.");
        } else {
            LOGGER.info("Loaded crawler configuration from: " + config.getFile().getAbsolutePath());
        }
    }

    private void initLinkInterceptors() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        final List<Object> interceptorObjs = config.getList("interceptors.interceptor[@class]");
        final ClassLoader myClassLoader = ClassLoader.getSystemClassLoader();
        for (Object obj : interceptorObjs) {
            final String clazz = (String)obj;
            Class myClass = myClassLoader.loadClass(clazz);
            final ILinkInterceptor linkInterceptor = (ILinkInterceptor)myClass.newInstance();
            linkInterceptors.add(linkInterceptor);
        }
    }

    private void initEndTasks() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        final List<Object> endTaskObjs = config.getList("endtasks.endtask[@class]");
        final ClassLoader myClassLoader = ClassLoader.getSystemClassLoader();
        for (Object obj : endTaskObjs) {
            final String clazz = (String)obj;
            Class myClass = myClassLoader.loadClass(clazz);
            final IEndTask endTask = (IEndTask)myClass.newInstance();
            endTasks.add(endTask);
        }
    }

    private void initInvalidUrlContents() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        final List<Object> contentObjs = config.getList("invalidurlcontents.content[@value]");
        for (Object obj : contentObjs) {
            final String value = (String)obj;
            invalidUrlContents.add(value);
        }
    }

    private void initSkippedExtensions() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        final List<Object> contentObjs = config.getList("skippedextensions.content[@value]");
        for (Object obj : contentObjs) {
            final String value = (String)obj;
            skippedExtensionContents.add(value);
        }
    }

    private void initSkippedUrls() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        final List<Object> contentObjs = config.getList("skippedurls.content[@value]");
        for (Object obj : contentObjs) {
            final String value = (String)obj;
            skippedUrlContents.add(value);
        }
    }
}
