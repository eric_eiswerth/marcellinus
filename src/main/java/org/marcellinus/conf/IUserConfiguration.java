package org.marcellinus.conf;

import java.util.List;
import org.apache.commons.configuration.ConfigurationException;
import org.marcellinus.crawler.endtasks.IEndTask;
import org.marcellinus.crawler.interceptors.ILinkInterceptor;

/**
 * Implementors of this interface should supply user configuration details.
 *
 * @author Eric Eiswerth
 */
public interface IUserConfiguration {

    public List<ILinkInterceptor> getLinkInterceptors();

    public List<IEndTask> getEndTasks();

    public List<String> getInvalidUrlContents();

    public List<String> getSkippedExtensions();

    public List<String> getSkippedUrls();

    public void load(ICrawlerConfiguration configuration) throws ConfigurationException;

}
