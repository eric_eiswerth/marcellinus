package org.marcellinus.exceptions;

/**
 * @author Eric Eiswerth
 */
public class CrawlerException extends Exception {

    public CrawlerException(String messsage, Throwable cause) {
        super(messsage, cause);
    }

}
