package org.marcellinus.exceptions;

/**
 * Thrown when there is an error while initializing the crawler.
 *
 * @author Eric Eiswerth
 */
public class InitializationException extends Exception {

    public InitializationException(String message) {
        super(message);
    }

}
