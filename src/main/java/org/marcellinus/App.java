package org.marcellinus;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import net.thucydides.core.webdriver.Configuration;
import net.thucydides.core.webdriver.SupportedWebDriver;
import net.thucydides.core.webdriver.WebdriverManager;
import org.apache.commons.io.FileUtils;
import org.marcellinus.conf.IUserConfiguration;
import org.marcellinus.crawler.Crawler;
import org.marcellinus.crawler.CrawlerConstants;
import org.marcellinus.crawler.endtasks.IEndTask;
import org.marcellinus.crawler.interceptors.ILinkInterceptor;
import org.marcellinus.exceptions.CrawlerException;
import org.marcellinus.exceptions.InitializationException;
import org.marcellinus.exceptions.ReportException;
import org.marcellinus.reporter.ReportDetails;
import org.marcellinus.reporter.Reporter;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.util.StopWatch;

/**
 * Main entry point for the crawler application.
 *
 * @author Eric Eiswerth
 */
public class App {

    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

    @Autowired
    private Configuration configuration;

    @Autowired
    private WebdriverManager webdriverManager;

    @Autowired
    private Crawler crawler;

    @Autowired
    private Reporter reporter;

    @Autowired
    private IUserConfiguration userConfiguration;

    @Autowired
    private File outputDir;

    @Autowired
    private File dbDir;

    @Autowired
    private File screenshotsDir;

    private boolean crawlerInitialized = false;

    public Configuration getConfiguration() {
        return configuration;
    }

    public WebdriverManager getWebDriverManager() {
        return webdriverManager;
    }

    public Crawler getCrawler() {
        if (!crawlerInitialized) {
            initCrawler();
            crawlerInitialized = true;
        }
        return crawler;
    }

    public Reporter getReporter() {
        return reporter;
    }

    public IUserConfiguration getUserConfiguration() {
        return userConfiguration;
    }

    private void cleanup() throws IOException {
        FileUtils.deleteDirectory(dbDir);
        FileUtils.deleteDirectory(screenshotsDir);
        FileUtils.deleteQuietly(new File(new File(outputDir, CrawlerConstants.CSS_DIR), "crawler.css"));
    }

    private void initCrawler() {
        if (userConfiguration != null) {
            final List<ILinkInterceptor> interceptors = userConfiguration.getLinkInterceptors();
            for (ILinkInterceptor interceptor : interceptors) {
                crawler.addLinkInterceptor(interceptor);
            }
            final List<IEndTask> endTasks = userConfiguration.getEndTasks();
            for (IEndTask endTask : endTasks) {
                crawler.addEndTask(endTask);
            }
        }
    }

    public static void main(String[] args) {
        final BeanFactory beanFactory = new AnnotationConfigApplicationContext("org.marcellinus");
        final App app = (App)beanFactory.getBean("app");

        final String baseUrl = app.getConfiguration().getBaseUrl();
        final SupportedWebDriver driver = app.getConfiguration().getDriverType();
        WebDriver webDriver = null;
        try {
            if (baseUrl == null || baseUrl.isEmpty()) {
                throw new InitializationException("The crawler.base.url property must be defined.");
            }
            app.cleanup();

            webDriver = app.getWebDriverManager().getWebdriver(driver.name());

            final StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            app.getCrawler().crawl(webDriver);
            stopWatch.stop();

            final ReportDetails reportDetails = new ReportDetails();
            final double minutes = stopWatch.getTotalTimeSeconds()/60;
            final DecimalFormat df = new DecimalFormat("0.#");

            reportDetails.setTotalTimeMinutes(Double.valueOf(df.format(minutes)).doubleValue());

            app.getReporter().report(reportDetails);
        } catch (InitializationException cie) {
            LOGGER.error("Error during initialization.", cie);
            System.exit(1);
        } catch (CrawlerException le) {
            LOGGER.error("Error visiting page.", le);
            System.exit(2);
        } catch (ReportException re) {
            LOGGER.error("Error during reporting.", re);
            System.exit(3);
        } catch (Exception e) {
            LOGGER.error("Crawler encountered an error.", e);
            System.exit(4);
        } finally {
            if (webDriver != null) {
                webDriver.close();
            }
        }
        System.exit(0);
    }

}
