package org.marcellinus.utils;

import java.io.IOException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.util.Objects;
import java.util.regex.Matcher;
import org.marcellinus.conf.ICrawlerConfiguration;
import org.marcellinus.crawler.CrawlerConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Various utility methods used by the crawler.
 *
 * @author Eric Eiswerth
 */
@Component
public class CrawlerUtils {

    private static final char[] BASE_URL_DELIMETERS = {'/', '?'};

    @Autowired
    private ICrawlerConfiguration configuration;

    /**
     * Gets whether or not two URLs are considered equivalent. URLs may appear to be different, but for the purposes of
     * the crawler, they may in fact be considered the same. For example, test.com and www.test.com are considered
     * equal. Essentially, this method determines whether or not the canonicalized representation of the URLs are equal.
     * @param url1
     * @param url2
     * @return
     */
    public boolean equalsUrl(String url1, String url2) {
        boolean result = false;
        if (Objects.equals(url1, url2)) {
            result = true;
        } else if (url1 != null && url2 != null) {
            url1 = cleanupUrl(url1);
            url2 = cleanupUrl(url2);
            result = url1.equalsIgnoreCase(url2);
        }
        return result;
    }

    /**
     * Gets the canonicalized format for the given URL. For example, if hashes are supported (see
     * {@link ICrawlerConfiguration#isHashSupported()} for more details) a different URL can actually map to the same
     * URL. For example, http://www.test.com, www.test.com, test.com, test.com#foo, etc... all map to test.com. This is
     * used to avoid visiting duplicate pages that are actually the same page.
     * @param url
     * @return
     */
    public String getCanonicalizedUrl(String url) {
        return cleanupUrl(url);
    }

    /**
     * Gets a hash of the given URL. This hash can be used to create unique keys from a URL. The given URL is
     * canonicalized before being hashed. See {@link CrawlerUtils#getCanonicalizedUrl(String)} for more details on
     * canonicalization of URLs.
     * @param url
     * @return
     */
    public String generateUniqueKeyFromUrl(String url) {
        final String canonicalUrl = getCanonicalizedUrl(url);
        try {
            final MessageDigest m = MessageDigest.getInstance("MD5");
            m.update(canonicalUrl.getBytes(), 0, canonicalUrl.length());
            return new BigInteger(1, m.digest()).toString(24);
        } catch (Exception e) {
            // Ignore.
        }
        return null;
    }

    /**
     * Strips the elements off the front of the given href. For example, given http://www.google.com, this method would
     * return google.com.
     * @param url
     * @return
     */
    public static String stripURLElements(String url) {
        Matcher matcher = CrawlerConstants.URL_PREFIX_PATTERN.matcher(url);
        while (matcher.find() && matcher.groupCount() == 2) {
            url = matcher.group(2);
            matcher = CrawlerConstants.URL_PREFIX_PATTERN.matcher(url);
        }
        return url;
    }

    /**
     * Gets the base URL. For example, if http://www.test.com/foo/test/foo.jsp is the given URL, the base URL would be
     * http://www.test.com
     * @param url
     * @return
     */
    public static String getBaseUrl(String url) {
        url = stripURLElements(url);
        for (char c : BASE_URL_DELIMETERS) {
            int index = url.indexOf(c);
            if (index != -1) {
                url = url.substring(0, index - 1);
            }
        }
        return url;
    }

    /**
     * Creates an absolute URL from a relative one. A relative URL, for our purposes, is one that does not start with
     * http://, https://, or www.
     * @param url
     * @return
     */
    public static String getAbsoluteUrl(String hostname, String url) {
        Matcher matcher = CrawlerConstants.URL_PREFIX_PATTERN.matcher(url);
        if (!matcher.matches()) {
            if (!url.startsWith("/")) {
                url = "/" + url;
            }
            url = hostname + url;
        }
        return url;
    }

    public static int getResponseCode(String url) throws IOException {
        final URL urlObj = new URL(url);
        final HttpURLConnection connection = (HttpURLConnection)urlObj.openConnection();
        connection.setConnectTimeout(5000);
        return connection.getResponseCode();
    }

    private String cleanupUrl(String url) {
        if (!configuration.isHashSupported()) {
            // Often there is no reason to consider named anchors when visiting links because they point to different
            // parts of the same page. Additionally, an incorrect named anchor does not result in any type of error.
            int index = url.indexOf("#");
            if (index != -1) {
                url = url.substring(0, index);
            }
        }
        if (url.endsWith("/")) {
            url = url.substring(0, url.length()-1);
        }
        url = stripURLElements(url);
        return url.trim();
    }

}
