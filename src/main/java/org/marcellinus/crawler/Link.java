package org.marcellinus.crawler;

/**
 * Simple POJO that stores a URL and the URL the brought us to the URL.
 *
 * @author Eric Eiswerth
 */
public class Link {

    private String fromUrl;

    private String url;

    private boolean artifact;

    public Link(String url, String fromUrl, boolean artifact) {
        this.url = url;
        this.fromUrl = fromUrl;
        this.artifact = artifact;
    }

    public String getFromUrl() {
        return fromUrl;
    }

    public String getUrl() {
        return url;
    }

    public boolean isArtifact() {
        return artifact;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Link)) {
            return false;
        }
        final Link link = (Link)obj;
        return getUrl().equals(link.getUrl());
    }

    @Override
    public int hashCode() {
        return getUrl().hashCode();
    }

    @Override
    public String toString() {
        return getUrl();
    }
}
