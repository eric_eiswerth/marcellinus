package org.marcellinus.crawler;

import java.util.*;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.marcellinus.conf.ICrawlerConfiguration;
import org.marcellinus.crawler.endtasks.IEndTask;
import org.marcellinus.crawler.interceptors.ILinkInterceptor;
import org.marcellinus.exceptions.CrawlerException;
import org.marcellinus.model.*;
import org.marcellinus.persistence.exceptions.EntityServiceException;
import org.marcellinus.persistence.service.IEntityService;
import org.marcellinus.utils.CrawlerUtils;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.MessageChannel;
import org.springframework.integration.message.GenericMessage;

/**
 * Performs the actual crawling.
 *
 * @author Eric Eiswerth
 */
public class Crawler {

    private static final Logger LOGGER = LoggerFactory.getLogger(Crawler.class);

    private static final List<ILinkInterceptor> INTERCEPTORs = new ArrayList<>();

    private static final List<IEndTask> END_TASKS = new ArrayList<>();

    @Autowired
    private ICrawlerConfiguration configuration;

    @Autowired
    private LinkValidator linkValidator;

    @Autowired
    private LinkVisitor linkVisitor;

    @Autowired
    private IEntityService<PageLinkEntity> pageLinkEntityService;

    @Autowired
    private LinkFactory linkFactory;

    @Autowired
    private PageView pageView;

    @Autowired
    private MessageChannel messageChannel;

    @Autowired
    private CrawlerUtils crawlerUtils;

    private WebDriver driver;

    private int maxLinksToCrawl = -1;

    private int numberOfLinksCrawled = 0;

    /**
     * Starts the crawler.
     * @param driver
     * @throws CrawlerException
     */
    public final void crawl(WebDriver driver) throws CrawlerException {
        Objects.requireNonNull(driver, "Web driver cannot be null.");
        this.driver = driver;
        this.maxLinksToCrawl = configuration.getMaxLinksToCrawl();
        if (maxLinksToCrawl != -1 && maxLinksToCrawl <= 0) {
            throw new IllegalArgumentException("Invalid setting for max links property: " + maxLinksToCrawl);
        }

        LOGGER.info("Begin crawling");
        // Start by opening the browser to the desired starting page.
        driver.navigate().to(configuration.getBaseUrl());
        visitUrls(visitUrl(new Link(configuration.getBaseUrl(), null, false)));
        LOGGER.info("Crawled " + numberOfLinksCrawled + " links");

        LOGGER.info("Firing end tasks");
        fireEndTasks();
        LOGGER.info("Finished crawling");
    }

    /**
     * Adds a link interceptor which will be called after a link is visited.
     * @param linkInterceptor
     */
    public void addLinkInterceptor(ILinkInterceptor linkInterceptor) {
        Objects.requireNonNull(linkInterceptor, "Link interceptor cannot be null.");
        INTERCEPTORs.add(linkInterceptor);
    }

    /**
     * Adds an end task which will be called after the crawler is finished.
     * @param endTask
     */
    public void addEndTask(IEndTask endTask) {
        Objects.requireNonNull(endTask, "End task cannot be null.");
        END_TASKS.add(endTask);
    }

    private void visitUrls(Set<Link> urls) throws CrawlerException {
        if (urls == null) {
            return;
        }
        for (Link link : urls) {
            visitUrls(visitUrl(link));
        }
    }

    private Set<Link> visitUrl(Link link) throws CrawlerException {
        if (!isValidLink(link) || !shouldContinueCrawling()) {
            return null;
        }
        LOGGER.info(link.toString());
        boolean collectLinks = visitPage(link);

        if (collectLinks) {
            return getLinks(link.getUrl());
        }
        return null;
    }

    private boolean visitPage(Link link) throws CrawlerException {
        String url = link.getUrl();
        // Immediately mark this URL as visited. If we get redirected somewhere else we want to make sure we don't
        // continue to this page over and over, only to get redirected each time.
        linkVisitor.markUrlAsVisited(url);

        final String originalUrl = url;

        final boolean artifact = link.isArtifact();
        if (!artifact) {
            driver.navigate().to(url);
            waitForRedirects();
            url = driver.getCurrentUrl();
        }

        final PageLinkEntity pageLinkEntity = linkFactory.getPageLinkEntity(url, originalUrl, artifact);
        boolean fireInterceptors = true;
        if (pageLinkEntity.isRedirected()) {
            LOGGER.info("Redirected to: " + url);
            boolean validExtension = linkValidator.isValidExtension(url);
            if (!isValidLink(new Link(url, link.getFromUrl(), !validExtension)) && !validExtension) {
                fireInterceptors = false;
            }
        }

        if (fireInterceptors) {
            fireLinkInterceptors(link.getFromUrl(), pageLinkEntity);
            linkVisitor.markUrlAsVisited(url);
            ++numberOfLinksCrawled;
        }
        return fireInterceptors;
    }

    private void waitForRedirects() {
        final int redirectWaitTime = configuration.getRedirectWaitTime();
        if (redirectWaitTime > 0) {
            // In the case of redirects, we need to sleep after clicking the element, otherwise the behaviour is not
            // what a user would see. Sleeping for 1 seconds seems to be sufficient, but is also an arbitrary value.
            try {
                Thread.sleep(configuration.getRedirectWaitTime());
            } catch (InterruptedException ie) {
                LOGGER.error("Error occurred while waiting for page to load.", ie);
            }
        }
    }

    private void fireLinkInterceptors(String fromUrl, PageLinkEntity pageLinkEntity) throws CrawlerException {
        try {
            final Pair<PageLinkEntity, LinkEntity> pair = getLinkEntity(fromUrl);
            if (pair != null) {
                // Add an incoming link to the current link
                pageLinkEntity.addIncomingLink(pair.getRight());
                if (!pair.getLeft().isArtifact()) {
                    pair.getLeft().addOutgoingLink(linkFactory.getLinkEntity(pageLinkEntity));
                    pageLinkEntityService.create(pair.getLeft());
                }
            }
            for (ILinkInterceptor linkInterceptor : INTERCEPTORs) {
                linkInterceptor.handleLink(pageLinkEntity, driver);
            }

            pageLinkEntityService.create(pageLinkEntity);
        } catch (EntityServiceException ese) {
            throw new CrawlerException("Error persisting link entity.", ese);
        }
    }

    private void fireEndTasks() {
        for (IEndTask endTask : END_TASKS) {
            endTask.completeEndTask(driver);
        }
    }

    private Pair<PageLinkEntity, LinkEntity> getLinkEntity(String url) throws EntityServiceException {
        if (url != null) {
            final LinkID linkID = linkFactory.getLinkID(url);
            final PageLinkEntity storedPageLinkEntity = pageLinkEntityService.read(linkID);
            if (storedPageLinkEntity != null) {
                return new ImmutablePair(storedPageLinkEntity,
                        linkFactory.getLinkEntity(storedPageLinkEntity));
            }
        }
        return null;
    }

    private Set<Link> getLinks(String fromUrl) {
        final Set<Link> links = new LinkedHashSet<>();
        final List<String> urls = pageView.getAllUrlsFromPage(driver);
        for (String url : urls) {
            if (isValidUrl(url)) {
                // A link is considered an artifact if the URL contains an invalid extension.
                links.add(new Link(url, fromUrl, !linkValidator.isValidExtension(url)));
            }
        }
        return links;
    }

    private boolean isValidUrl(String url) {
        boolean result = false;
        if (url != null && !url.isEmpty()) {
            result = linkValidator.isValid(url);
            if (!result) {
                final SkippedLink skippedLink = linkFactory.getSkippedLink(url);
                messageChannel.send(new GenericMessage(skippedLink));
            }
        }
        return result;
    }

    private boolean isValidLink(Link link) {
        boolean result = false;
        final String url = link.getUrl();
        final boolean validUrl = isValidUrl(url);
        if (validUrl) {
            boolean visitedUrl = linkVisitor.hasVisitedUrl(url);
            if (visitedUrl) {
                updateLinks(link);
            }
            result = !visitedUrl;
        }
        return result;
    }

    private void updateLinks(Link link) {
        final LinkID linkID = linkFactory.getLinkID(link.getUrl());
        try {
            // Need to retrieve the previously visited link from the DB so we can update its references.
            final PageLinkEntity storedPageLinkEntity = pageLinkEntityService.read(linkID);
            if (storedPageLinkEntity != null) {
                final String refUrl = link.getFromUrl();
                if (!crawlerUtils.equalsUrl(storedPageLinkEntity.getUrl(), refUrl)) {
                    final Pair<PageLinkEntity, LinkEntity> pair = getLinkEntity(refUrl);
                    if (pair != null) {
                        storedPageLinkEntity.addIncomingLink(pair.getRight());
                        if (!pair.getLeft().isArtifact()) {
                            pair.getLeft().addOutgoingLink(linkFactory.getLinkEntity(storedPageLinkEntity));
                            pageLinkEntityService.create(pair.getLeft());
                        }
                        pageLinkEntityService.create(storedPageLinkEntity);
                    }
                }
            }
        } catch (EntityServiceException ese) {
            LOGGER.error("Error retrieving link info: " + linkID, ese);
        }
    }

    private boolean shouldContinueCrawling() {
        return !(maxLinksToCrawl != -1 && numberOfLinksCrawled >= maxLinksToCrawl);
    }

}
