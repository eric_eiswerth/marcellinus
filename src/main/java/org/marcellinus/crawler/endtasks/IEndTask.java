package org.marcellinus.crawler.endtasks;

import org.openqa.selenium.WebDriver;

/**
 * @author Eric Eiswerth
 */
public interface IEndTask {

    public void completeEndTask(WebDriver driver);

}
