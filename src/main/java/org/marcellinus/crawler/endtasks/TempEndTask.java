package org.marcellinus.crawler.endtasks;

import org.openqa.selenium.WebDriver;

/**
 * @author Eric Eiswerth
 */
public class TempEndTask implements IEndTask {

    @Override
    public void completeEndTask(WebDriver driver) {
        driver.navigate().to("https://www.telus.com/identity/nf/logout.do");
    }
}
