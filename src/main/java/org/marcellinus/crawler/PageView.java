package org.marcellinus.crawler;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.stereotype.Component;

/**
 * The page view is responsible for retrieving elements from the current page so that the crawler can consume the
 * elements.
 *
 * @author Eric Eiswerth
 */
@Component("pageView")
public class PageView implements IPageView {

    @Override
    public List<String> getAllUrlsFromPage(WebDriver driver) {
        final List<String> urls = new ArrayList<>();
        final List<WebElement> linkElements = driver.findElements(By.tagName("a"));
        if (linkElements != null) {
            for (WebElement element : linkElements) {
                if (isValidElement(element)) {
                    urls.add(getUrl(element));
                }
            }
        }
        return urls;
    }

    @Override
    public List<String> getAllImageSourcesFromPage(WebDriver driver) {
        final List<String> urls = new ArrayList<>();
        final List<WebElement> linkElements = driver.findElements(By.tagName("img"));
        if (linkElements != null) {
            for (WebElement element : linkElements) {
                final String url = getImageSource(element);
                if (url != null) {
                    urls.add(url);
                }
            }
        }
        return urls;
    }

    private String getImageSource(WebElement element) {
        return element.getAttribute("src");
    }

    private String getUrl(WebElement element) {
        return element.getAttribute("href");
    }

    private boolean isValidElement(WebElement element) {
        return element.isDisplayed() && element.isEnabled();
    }

}
