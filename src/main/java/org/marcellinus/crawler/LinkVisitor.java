package org.marcellinus.crawler;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import org.marcellinus.utils.CrawlerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The LinkVisitor keeps track of which links have been visited.
 *
 * @author Eric Eiswerth
 */
@Component("linkVisitor")
public class LinkVisitor {

    private final Set<String> links = new HashSet<>();

    @Autowired
    private CrawlerUtils crawlerUtils;

    /**
     * Marks the given URL as visited.
     * @param url The URL being visited.
     */
    public void markUrlAsVisited(String url) {
        Objects.requireNonNull(url, "URL cannot be null.");

        url = crawlerUtils.getCanonicalizedUrl(url);
        links.add(url);
    }

    /**
     * Returns whether or not the given URL has been visited already.
     * @param url
     * @return
     */
    public boolean hasVisitedUrl(String url) {
        Objects.requireNonNull(url, "URL cannot be null.");

        url = crawlerUtils.getCanonicalizedUrl(url);
        return links.contains(url);
    }

    /**
     * Gets the number of links that were visited.
     * @return
     */
    public int getNumberOfLinksVisited() {
        return links.size();
    }

    /**
     * Gets all the links that were visited.
     * @return
     */
    public Set<String> getLinks() {
        return Collections.unmodifiableSet(links);
    }

}