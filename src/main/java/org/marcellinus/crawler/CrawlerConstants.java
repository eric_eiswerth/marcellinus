package org.marcellinus.crawler;

import java.util.regex.Pattern;

/**
 * Various constants for the crawler.
 *
 * @author Eric Eiswerth
 */
public abstract class CrawlerConstants {

    public static final Pattern URL_PREFIX_PATTERN = Pattern.compile("^(http://|https://|www[0-9]*.)(.*$)");

    public static final String HTTP_PREFIX = "http://";

    public static final String HTTPS_PREFIX = "https://";

    public static final String WWW_PREFIX = "www";

    public static final String SCREEN_SHOTS_DIR = "crawlershots";

    public static final String IMAGES_DIR = "images";

    public static final String REPORT_RESOURCES_DIR = "crawler-report-resources";

    public static final String CSS_DIR = "css";

}
