package org.marcellinus.crawler;

import java.util.List;
import org.openqa.selenium.WebDriver;

/**
 * @author Eric Eiswerth
 */
public interface IPageView {

    /**
     * Gets all the urls that are present on the current page. The anchor elements must be displayed and enabled on the
     * page in order to be returned in the resulting collection.
     * @param driver
     * @return
     */
    public List<String> getAllUrlsFromPage(WebDriver driver);

    /**
     * Gets all the image source URLs present on the current page. The image elements must be displayed and enabled on
     * the page in order to be returned in the resulting collection.
     * @param driver
     * @return
     */
    public List<String> getAllImageSourcesFromPage(WebDriver driver);

}
