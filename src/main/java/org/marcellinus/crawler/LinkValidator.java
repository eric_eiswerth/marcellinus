package org.marcellinus.crawler;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.marcellinus.utils.CrawlerUtils;

/**
 * The LinkValidator validates links.
 *
 * @author Eric Eiswerth
 */
public class LinkValidator {

    private final Set<String> skippedLinksSet = new HashSet<>();
    private final Set<String> skippedLinkContentsSet = new HashSet<>();
    private final Set<String> skippedExtensionsSet = new HashSet<>();

    /**
     * The base baseUrl for the tests (e.g., telus.com).
     */
    private String baseUrl;

    /**
     * Safe baseUrl (i.e., stripped of HTTP prefixes).
     */
    private String safeBaseUrl;

    /**
     * The regular expression for validating links.
     */
    private Pattern urlPattern;

    /**
     * Creates a new {@link LinkValidator} with the given baseUrl.
     * @param baseUrl
     */
    public LinkValidator(String baseUrl) {
        Objects.requireNonNull(baseUrl, "Base URL cannot be null.");
        this.baseUrl = baseUrl;
        this.safeBaseUrl = CrawlerUtils.getBaseUrl(baseUrl);
        init();
    }

    private void init() {
        urlPattern = Pattern.compile("(" + CrawlerConstants.HTTP_PREFIX + "|" + CrawlerConstants.HTTPS_PREFIX + ")?(" +
                CrawlerConstants.WWW_PREFIX + "[0-9]*.)?(" + safeBaseUrl + "){1}(/)?.*");
    }

    /**
     * Gets whether or not the given URL is valid.
     * @param url
     * @return
     */
    public boolean isValid(String url) {
        if (url != null && !url.isEmpty()) {
            if (url.endsWith("/")) {
                url = url.substring(0, url.length()-1);
            }
            url = CrawlerUtils.getAbsoluteUrl(baseUrl, url);
            return isValidLinkContent(url) &&
                    !skippedLinksSet.contains(url) &&
                    isValidURL(url);
        }
        return false;
    }

    public void addInvalidUrlContent(String value) {
        skippedLinkContentsSet.add(value);
    }

    public void addSkippedExtension(String value) {
        skippedExtensionsSet.add(value);
    }

    public void addSkippedUrl(String value) {
        skippedLinksSet.add(value);
    }

    public boolean isValidExtension(String url) {
        // Strip off any query parameters for extensions since we don't really want to visit the links anyway.
        int queryIndex = url.indexOf("?");
        if (queryIndex != -1) {
            // Contains query params
            int slashIndex = url.lastIndexOf("/");
            if (slashIndex != -1 && slashIndex < queryIndex) {
                // pull out the base URL
                url = url.substring(slashIndex + 1, queryIndex);
            }
        }
        for (String extension : skippedExtensionsSet) {
            if (url.endsWith(extension)) {
                return false;
            }
        }
        return true;
    }

    private boolean isValidLinkContent(String href) {
        for (String content : skippedLinkContentsSet) {
            if (href.contains(content)) {
                return false;
            }
        }
        return true;
    }

    private boolean isValidURL(String url) {
        Matcher matcher = urlPattern.matcher(url);
        return matcher.matches();
    }

}
