package org.marcellinus.crawler.interceptors;

import org.marcellinus.model.PageLinkEntity;
import org.marcellinus.exceptions.CrawlerException;
import org.openqa.selenium.WebDriver;

/**
 * @author Eric Eiswerth
 */
public interface ILinkInterceptor {

    public void handleLink(PageLinkEntity pageLinkEntity, WebDriver driver) throws CrawlerException;

}
