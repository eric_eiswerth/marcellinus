package org.marcellinus.crawler.interceptors;

import java.io.IOException;
import java.util.List;
import org.marcellinus.crawler.IPageView;
import org.marcellinus.exceptions.CrawlerException;
import org.marcellinus.model.ImageLink;
import org.marcellinus.model.LinkFactory;
import org.marcellinus.model.PageLinkEntity;
import org.marcellinus.utils.CrawlerUtils;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Responsible for setting the response codes of the images on a given page.
 */
public class ImageLinkInterceptor implements ILinkInterceptor {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImageLinkInterceptor.class);

    @Autowired
    private IPageView pageView;

    @Autowired
    private LinkFactory linkFactory;

    @Override
    public void handleLink(PageLinkEntity pageLinkEntity, WebDriver driver) throws CrawlerException {
        if (pageLinkEntity.isArtifact()) {
            return;
        }
        final List<String> links = pageView.getAllImageSourcesFromPage(driver);
        for (String link : links) {
            int responseCode = -1;
            try {
                responseCode = CrawlerUtils.getResponseCode(link);
            } catch (IOException ioe) {
                LOGGER.error("Error retrieving image status: " + link, ioe);
            }
            final ImageLink imageLink = linkFactory.getImageLink(link, responseCode);
            pageLinkEntity.addImageLink(imageLink);
        }
    }
}
