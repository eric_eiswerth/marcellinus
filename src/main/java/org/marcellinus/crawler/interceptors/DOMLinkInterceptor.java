package org.marcellinus.crawler.interceptors;

import org.apache.commons.lang3.StringEscapeUtils;
import org.marcellinus.exceptions.CrawlerException;
import org.marcellinus.model.PageLinkEntity;
import org.openqa.selenium.WebDriver;

/**
 * @author Eric Eiswerth
 */
public class DOMLinkInterceptor implements ILinkInterceptor {

    @Override
    public void handleLink(PageLinkEntity pageLinkEntity, WebDriver driver) throws CrawlerException {
        if (!pageLinkEntity.isArtifact()) {
            String pageSource = driver.getPageSource();
            pageSource = StringEscapeUtils.escapeXml(pageSource);
            pageLinkEntity.setPageSource(pageSource);
        }
    }
}
