package org.marcellinus.crawler.interceptors;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import net.thucydides.core.webdriver.Configuration;
import org.apache.commons.io.FileUtils;
import org.marcellinus.crawler.CrawlerConstants;
import org.marcellinus.model.PageLinkEntity;
import org.marcellinus.exceptions.CrawlerException;
import org.imgscalr.Scalr;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Interceptor that handles the storing of the screenshots.
 *
 * @author Eric Eiswerth
 */
public class ScreenshotLinkInterceptor implements ILinkInterceptor {

    @Autowired
    private Configuration configuration;

    @Autowired
    private File screenshotsDir;

    @Override
    public void handleLink(PageLinkEntity pageLinkEntity, WebDriver driver) throws CrawlerException {
        String screenshotThumbPath = null;
        String screenshotPath = null;
        String description = null;
        if (pageLinkEntity.isArtifact()) {
            screenshotThumbPath = CrawlerConstants.IMAGES_DIR + File.separator + "artifact_thumb.png";
            screenshotPath = CrawlerConstants.IMAGES_DIR + File.separator + "artifact.png";
        } else {
            try {
                final File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                if (file != null && file.isFile()) {
                    final File destFile = new File(screenshotsDir, file.getName());
                    FileUtils.copyFile(file, destFile);

                    final File thumbDestFile = createThumbnail(destFile);

                    screenshotThumbPath = CrawlerConstants.SCREEN_SHOTS_DIR + File.separator + thumbDestFile.getName();
                    screenshotPath = CrawlerConstants.SCREEN_SHOTS_DIR + File.separator + destFile.getName();
                }
            } catch (IOException ioe) {
                throw new CrawlerException("Error copying screenshot.", ioe);
            }
        }
        pageLinkEntity.setScreenshotThumbPath(screenshotThumbPath);
        pageLinkEntity.setScreenshotPath(screenshotPath);
        pageLinkEntity.setDescription(description);
    }

    private File createThumbnail(File originalFile) throws IOException {
        BufferedImage img = ImageIO.read(originalFile);
        // Create quickly, then smooth and brighten it.
        img = Scalr.resize(img, 100);

        final File thumbFile = new File(screenshotsDir, "thumb_" + originalFile.getName());
        ImageIO.write(img, "png", thumbFile);
        return thumbFile;
    }
}
