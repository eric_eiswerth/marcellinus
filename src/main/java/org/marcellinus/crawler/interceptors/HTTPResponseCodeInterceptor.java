package org.marcellinus.crawler.interceptors;

import java.io.IOException;
import java.net.MalformedURLException;
import org.marcellinus.model.PageLinkEntity;
import org.marcellinus.exceptions.CrawlerException;
import org.marcellinus.utils.CrawlerUtils;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HTTPResponseCodeInterceptor implements ILinkInterceptor {

    private static final Logger LOGGER = LoggerFactory.getLogger(HTTPResponseCodeInterceptor.class);

    @Override
    public void handleLink(PageLinkEntity pageLinkEntity, WebDriver driver) throws CrawlerException {
        try {
            final int responseCode = CrawlerUtils.getResponseCode(pageLinkEntity.getUrl());
            pageLinkEntity.setResponseCode(responseCode);
        } catch (MalformedURLException murle) {
            LOGGER.error("Error getting HTTP status code.", murle);
        } catch (IOException ioe) {
            LOGGER.error("Error getting HTTP status code.", ioe);
        }
    }
}
