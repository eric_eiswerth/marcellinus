package org.marcellinus.crawler.interceptors;

import java.util.HashMap;
import java.util.Map;
import org.marcellinus.model.PageLinkEntity;
import org.marcellinus.exceptions.CrawlerException;
import org.marcellinus.pages.ICrawlerPage;
import org.marcellinus.pages.TempPage;
import org.openqa.selenium.WebDriver;

/**
 * @author Eric Eiswerth
 */
public class PageActionLinkInterceptor implements ILinkInterceptor {

    private static final Map<String, ICrawlerPage> pages = new HashMap<>();

    static {
        pages.put("http://www.telus.com/regionselect.html", new TempPage());
    }

    @Override
    public void handleLink(PageLinkEntity pageLinkEntity, WebDriver driver) throws CrawlerException {
        final ICrawlerPage page = pages.get(pageLinkEntity.getUrl());
        if (page != null) {
            page.performActions(driver);
        }
    }
}
