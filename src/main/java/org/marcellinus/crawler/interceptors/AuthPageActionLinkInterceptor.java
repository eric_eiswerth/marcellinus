package org.marcellinus.crawler.interceptors;

import java.util.HashMap;
import java.util.Map;
import org.marcellinus.exceptions.CrawlerException;
import org.marcellinus.model.PageLinkEntity;
import org.marcellinus.pages.ICrawlerPage;
import org.marcellinus.pages.LoginPage;
import org.openqa.selenium.WebDriver;

public class AuthPageActionLinkInterceptor implements ILinkInterceptor {

    private static final Map<String, ICrawlerPage> pages = new HashMap<>();

    static {
        pages.put("http://www.telus.com/customermgt/yourAccounts.do", new LoginPage());
    }

    @Override
    public void handleLink(PageLinkEntity pageLinkEntity, WebDriver driver) throws CrawlerException {
        final String fromUrl = pageLinkEntity.getRedirectedFromUrl();
        if (fromUrl != null) {
            final ICrawlerPage page = pages.get(fromUrl);
            if (page != null) {
                page.performActions(driver);
            }
        }
    }
}
