package org.marcellinus.messages.handlers;

import org.marcellinus.model.SkippedLink;
import org.marcellinus.persistence.exceptions.EntityServiceException;
import org.marcellinus.persistence.service.SkippedLinkEntityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.Message;
import org.springframework.integration.MessagingException;
import org.springframework.integration.core.MessageHandler;

/**
 * This class is responsible for logging skipped links to the database.
 *
 * @author Eric Eiswerth
 */
public class SkippedLinkDBMessageHandler implements MessageHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(SkippedLinkDBMessageHandler.class);

    @Autowired
    private SkippedLinkEntityService skippedLinkEntityService;

    @Override
    public void handleMessage(Message<?> message) throws MessagingException {
        if (message != null) {
            final SkippedLink skippedLink = (SkippedLink)message.getPayload();
            saveSkippedLink(skippedLink);
        }
    }

    private void saveSkippedLink(SkippedLink skippedLink) {
        try {
            skippedLinkEntityService.create(skippedLink);
        } catch (EntityServiceException ess) {
            LOGGER.error("Error saving skipped link to DB: " + skippedLink, ess);
        }
    }
}
