package org.marcellinus.messages.handlers;

import org.marcellinus.model.SkippedLink;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.Message;
import org.springframework.integration.MessagingException;
import org.springframework.integration.core.MessageHandler;

/**
 * This class is responsible for logging skipped links to a log file.
 *
 * @author Eric Eiswerth
 */
public class SkippedLinkLoggerMessageHandler implements MessageHandler {

    private static final Logger SKIPPED_LINKS_LOGGER = LoggerFactory.getLogger("skipped-links-logger");

    @Override
    public void handleMessage(Message<?> message) throws MessagingException {
        if (message != null) {
            final SkippedLink skippedLink = (SkippedLink)message.getPayload();
            SKIPPED_LINKS_LOGGER.info(skippedLink.getUrl());
        }
    }
}
