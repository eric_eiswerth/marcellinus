package org.marcellinus.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is a test page that shows what a potential {@link ICrawlerPage} implementation looks like.
 *
 * @author Eric Eiswerth
 */
public class TempPage implements ICrawlerPage {

    private static final Logger LOGGER = LoggerFactory.getLogger(TempPage.class);

    @Override
    public void performActions(WebDriver driver) {
        final WebElement englishRadioButton = driver.findElement(By.id("english-en"));
        final WebElement personalUseRadioButton = driver.findElement(By.id("personalUse-en"));
        final WebElement abRegionElement = driver.findElement(By.className("AB"));

        englishRadioButton.click();
        personalUseRadioButton.click();
        abRegionElement.click();

        // In the case of redirects, we need to sleep after clicking the element, otherwise the behaviour is not
        // what a user would see. Sleeping for 1 seconds seems to be sufficient, but is also an arbitrary value.
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ie) {
            LOGGER.error("Error occurred while waiting for page to load.", ie);
        }
    }
}
