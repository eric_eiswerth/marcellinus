package org.marcellinus.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage implements ICrawlerPage{

    public void performActions(WebDriver driver) {
        final String username = System.getProperty("username");
        final String password = System.getProperty("password");
        if (username == null || username.isEmpty() || password == null || password.isEmpty()) {
            throw new RuntimeException("Username and/or password is not set.");
        }

        final WebElement usernameInput = driver.findElement(By.id("IDToken1"));
        final WebElement passwordInput = driver.findElement(By.id("IDToken2"));
        final WebElement loginButton = driver.findElement(By.id("butUpdate"));

        usernameInput.sendKeys(username);
        passwordInput.sendKeys(password);
        loginButton.submit();

        // It is assumed that the actions that were performed by the page resulted in clicking on something that takes
        // the user to a new page.  We sleep for a couple seconds to give the new page time to render.
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ie) {
            // Do nothing
        }
    }
}
