package org.marcellinus.pages;

import org.openqa.selenium.WebDriver;

/**
 * @author Eric Eiswerth
 */
public interface ICrawlerPage {

    public abstract void performActions(WebDriver driver);

}