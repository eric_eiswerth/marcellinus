Marcellinus

This project is an abstraction on top of Thucydides. It honors many of the same properties that Thucydides accepts (i.e., screen resolution, input and output directories, Saucelabs integration, etc...).

The name, Marcellinus, comes from Greek history.  Marcellinus was a Greek historian who wrote about Thucydides.

To use, make sure the following repository is in your pom.xml:

    <repositories>
        <repository>
            <id>public-repo</id>
            <name>Public</name>
            <url>http://174.129.226.138:8080/nexus/content/groups/public/</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
    </repositories>

Then simply add the following dependency:

    <dependency>
        <groupId>org.marcellinus</groupId>
        <artifactId>crawler</artifactId>
        <version>1.0.2-SNAPSHOT</version>
     </dependency>
